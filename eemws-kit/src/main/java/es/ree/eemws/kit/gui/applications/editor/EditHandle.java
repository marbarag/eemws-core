/*
 * Copyright 2024 Redeia.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTIBIILTY or FITNESS FOR A PARTICULAR PURPOSE. See GNU Lesser General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see
 * http://www.gnu.org/licenses/.
 *
 * Any redistribution and/or modification of this program has to make
 * reference to Redeia as the copyright owner of the program.
 */

package es.ree.eemws.kit.gui.applications.editor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.Serializable;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.WindowConstants;

import es.ree.eemws.core.utils.xml.XMLUtil;
import es.ree.eemws.kit.gui.common.Constants;
import es.ree.eemws.kit.gui.common.Logger;

/**
 * Edition management.
 *
 * @author Redeia.
 * @version 2.1 01/01/2024
 */
public final class EditHandle implements Serializable {

	/** Serial version UID. */
	private static final long serialVersionUID = -4929262114891652853L;

	/** Reference to document Handle. */
	private DocumentHandle documentHandle;

	/** Log window. */
	private Logger log;

	/** Last search term. */
	private String lastSearchTerm = null;

	/** Indicate whether search must ignores capitalization. */
	private boolean findIsCaseSensitive = false;

	/** Main window. */
	private Editor mainWindow = null;

	/** Edit menu. */
	private JMenu editMenu = new JMenu();

	/** Button bar. */
	private JToolBar buttonBar = new JToolBar();

	/**
	 * Constructor. Creates a new instance of Text Manager.
	 *
	 * @param window Reference to main window.
	 */
	public EditHandle(final Editor window) {

		mainWindow = window;
		documentHandle = mainWindow.getDocumentHandle();
		log = mainWindow.getLogHandle().getLog();
	}

	/**
	 * Retrieves "Edit" menu.
	 *
	 * @return Edition menu for Main Options bar.
	 */
	public JMenu getMenu() {

		var cut = documentHandle.getAction("cut-to-clipboard"); //$NON-NLS-1$
		var cutMenuItem = new JMenuItem(cut);
		cutMenuItem.setAccelerator(KeyStroke.getKeyStroke('X', InputEvent.CTRL_DOWN_MASK));
		cutMenuItem.setText(MessageCatalog.EDITOR_CUT.getMessage());
		cutMenuItem.setMnemonic(MessageCatalog.EDITOR_CUT_HK.getChar());

		var copy = documentHandle.getAction("copy-to-clipboard"); //$NON-NLS-1$
		var copyMenuItem = new JMenuItem(copy);
		copyMenuItem.setAccelerator(KeyStroke.getKeyStroke('C', InputEvent.CTRL_DOWN_MASK));
		copyMenuItem.setText(MessageCatalog.EDITOR_COPY.getMessage());
		copyMenuItem.setMnemonic(MessageCatalog.EDITOR_COPY_HK.getChar());

		var paste = documentHandle.getAction("paste-from-clipboard"); //$NON-NLS-1$
		var pasteMenuItem = new JMenuItem(paste);
		pasteMenuItem.setAccelerator(KeyStroke.getKeyStroke('V', InputEvent.CTRL_DOWN_MASK));
		pasteMenuItem.setText(MessageCatalog.EDITOR_PASTE.getMessage());
		pasteMenuItem.setMnemonic(MessageCatalog.EDITOR_PASTE_HK.getChar());

		var findMenuItem = new JMenuItem(MessageCatalog.EDITOR_FIND.getMessage(),
		        new ImageIcon(getClass().getResource(Constants.ICON_FIND)));
		findMenuItem.setMnemonic(MessageCatalog.EDITOR_FIND_HK.getChar());
		findMenuItem.setAccelerator(
		        KeyStroke.getKeyStroke(MessageCatalog.EDITOR_FIND_HK.getChar(), InputEvent.CTRL_DOWN_MASK));
		findMenuItem.addActionListener(e -> find());

		var findNextMenuItem = new JMenuItem(MessageCatalog.EDITOR_FIND_NEXT.getMessage(),
		        new ImageIcon(getClass().getResource(Constants.ICON_FIND)));
		findNextMenuItem.setMnemonic(MessageCatalog.EDITOR_FIND_NEXT.getChar());
		findNextMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F3, 0));
		findNextMenuItem.addActionListener(e -> findNext(true));

		var replaceMenuItem = new JMenuItem();
		replaceMenuItem.setText(MessageCatalog.EDITOR_REPLACE.getMessage());
		replaceMenuItem.setMnemonic(MessageCatalog.EDITOR_REPLACE_HK.getChar());
		replaceMenuItem.setAccelerator(
		        KeyStroke.getKeyStroke(MessageCatalog.EDITOR_REPLACE_HK.getChar(), InputEvent.CTRL_DOWN_MASK));
		replaceMenuItem.addActionListener(e -> replace());

		var goToLineMenuItem = new JMenuItem(MessageCatalog.EDITOR_GO_TO_LINE.getMessage(),
		        new ImageIcon(getClass().getResource(Constants.ICON_GO)));
		goToLineMenuItem.setMnemonic(MessageCatalog.EDITOR_GO_TO_LINE_HK.getChar());
		goToLineMenuItem.setAccelerator(
		        KeyStroke.getKeyStroke(MessageCatalog.EDITOR_GO_TO_LINE_HK.getChar(), InputEvent.CTRL_DOWN_MASK));
		goToLineMenuItem.addActionListener(e -> goToLine());

		var undoMenuItem = new JMenuItem(mainWindow.getUndoRedoHandle().getUndoAction());
		undoMenuItem.setAccelerator(KeyStroke.getKeyStroke('Z', InputEvent.CTRL_DOWN_MASK));
		undoMenuItem.setText(MessageCatalog.EDITOR_UNDO.getMessage());
		undoMenuItem.setMnemonic(MessageCatalog.EDITOR_UNDO_HK.getChar());

		var redoMenuItem = new JMenuItem(mainWindow.getUndoRedoHandle().getRedoAction());
		redoMenuItem.setAccelerator(KeyStroke.getKeyStroke('Y', InputEvent.CTRL_DOWN_MASK));
		redoMenuItem.setText(MessageCatalog.EDITOR_REDO.getMessage());
		redoMenuItem.setMnemonic(MessageCatalog.EDITOR_REDO.getChar());

		var selectAll = documentHandle.getAction("select-all"); //$NON-NLS-1$
		var selectAllMenuItem = new JMenuItem(selectAll);
		selectAllMenuItem.setAccelerator(KeyStroke.getKeyStroke('A', InputEvent.CTRL_DOWN_MASK));
		selectAllMenuItem.setText(MessageCatalog.EDITOR_SELECT_ALL.getMessage());
		selectAllMenuItem.setMnemonic(MessageCatalog.EDITOR_SELECT_ALL_HK.getChar());

		var selectLine = documentHandle.getAction("select-line"); //$NON-NLS-1$
		var selectLineMenuItem = new JMenuItem(selectLine);
		selectLineMenuItem.setText(MessageCatalog.EDITOR_SELECT_LINE.getMessage());
		selectLineMenuItem.setMnemonic(MessageCatalog.EDITOR_SELECT_LINE_HK.getChar());

		var xmlFormatMenuItem = new JMenuItem();
		xmlFormatMenuItem.setText(MessageCatalog.EDITOR_MENU_ITEM_XML_FORMAT.getMessage());
		xmlFormatMenuItem.setMnemonic(MessageCatalog.EDITOR_MENU_ITEM_XML_FORMAT_HK.getChar());
		xmlFormatMenuItem.addActionListener(e -> applyFormat());

		editMenu.setText(MessageCatalog.EDITOR_EDIT_MENU_ENTRY.getMessage());
		editMenu.setMnemonic(MessageCatalog.EDITOR_EDIT_MENU_ENTRY_HK.getChar());
		editMenu.add(cutMenuItem);
		editMenu.add(copyMenuItem);
		editMenu.add(pasteMenuItem);
		editMenu.addSeparator();
		editMenu.add(findMenuItem);
		editMenu.add(findNextMenuItem);
		editMenu.add(replaceMenuItem);
		editMenu.add(goToLineMenuItem);
		editMenu.addSeparator();
		editMenu.add(undoMenuItem);
		editMenu.add(redoMenuItem);
		editMenu.addSeparator();
		editMenu.add(selectAllMenuItem);
		editMenu.add(selectLineMenuItem);
		editMenu.addSeparator();
		editMenu.add(xmlFormatMenuItem);

		return editMenu;
	}

	/**
	 * Applies format (tabs, spaces, etc.) on current document.
	 */
	private void applyFormat() {
		if (documentHandle.isEmpty()) {
			JOptionPane.showMessageDialog(mainWindow, MessageCatalog.EDITOR_DOCUMENT_EMPTY.getMessage(),
			        es.ree.eemws.kit.gui.common.MessageCatalog.MSG_INFO_TITLE.getMessage(),
			        JOptionPane.INFORMATION_MESSAGE);
		} else {
			mainWindow.enableScreen(false);
			documentHandle.openReversible(XMLUtil.prettyPrint(documentHandle.getPlainText()));
			mainWindow.enableScreen(true);
		}
	}

	/**
	 * Retrieves Edition Button bar.
	 *
	 * @return Edition Button bar.
	 */
	public JToolBar getButtonBar() {

		buttonBar.setFloatable(true);

		var goToLineBtn = new JButton();
		goToLineBtn.setIcon(new ImageIcon(getClass().getResource(Constants.ICON_GO)));
		goToLineBtn.setToolTipText(MessageCatalog.EDITOR_GO_TO_LINE.getMessage());
		goToLineBtn.setBorderPainted(false);
		goToLineBtn.addActionListener(e -> goToLine());

		var findBtn = new JButton();
		findBtn.setIcon(new ImageIcon(getClass().getResource(Constants.ICON_FIND)));
		findBtn.setToolTipText(MessageCatalog.EDITOR_FIND.getMessage());
		findBtn.setBorderPainted(false);
		findBtn.addActionListener(e -> find());

		var btApplyFormat = new JButton();
		btApplyFormat.setIcon(new ImageIcon(getClass().getResource(Constants.ICON_FORMAT)));
		btApplyFormat.setToolTipText(MessageCatalog.EDITOR_MENU_ITEM_XML_FORMAT.getMessage());
		btApplyFormat.setBorderPainted(false);
		btApplyFormat.addActionListener(e -> applyFormat());

		buttonBar.add(goToLineBtn, null);
		buttonBar.add(findBtn, null);
		buttonBar.add(btApplyFormat, null);

		return buttonBar;
	}

	/**
	 * Enables disable graphic values.
	 *
	 * @param activeValue <code>true</code> enable. <code>false</code> disable.
	 */
	public void enable(final boolean activeValue) {
		for (Component component : buttonBar.getComponents()) {
			component.setEnabled(activeValue);
		}

		for (Component menu : editMenu.getMenuComponents()) {
			menu.setEnabled(activeValue);
		}
	}

	/**
	 * Searches successively in document the last term entered, starting from cursor
	 * position.
	 *
	 * @param mustShowNotFoundDialogue Indicate whether the "not found" dialogue
	 *                                 must be displayed or not.
	 * @return <code>true</code> If search term was found. <code>false</code>
	 *         Otherwise.
	 */
	private boolean findNext(final boolean mustShowNotFoundDialogue) {

		var retValue = false;
		if (!documentHandle.isEmpty()) {

			if (lastSearchTerm != null) {

				var search = lastSearchTerm;
				var text = documentHandle.getPlainText();
				if (!findIsCaseSensitive) {

					text = text.toLowerCase();
					search = search.toLowerCase();
				}

				var chrStart = documentHandle.getCursorPosition();
				var start = text.indexOf(search, chrStart);
				if (start == -1) {

					if (mustShowNotFoundDialogue) {

						var msg = MessageCatalog.EDITOR_SEARCH_NOT_FOUND_DETAIL.getMessage(lastSearchTerm, chrStart);

						JOptionPane.showMessageDialog(mainWindow, msg,
						        MessageCatalog.EDITOR_SEARCH_NOT_FOUND.getMessage(), JOptionPane.INFORMATION_MESSAGE);
						log.logMessage(msg);
					}

				} else {

					var end = lastSearchTerm.length() + start;
					documentHandle.markText(start, end);
					retValue = true;
				}

			} else {

				find();
			}

		} else {

			JOptionPane.showMessageDialog(mainWindow, MessageCatalog.EDITOR_DOCUMENT_EMPTY.getMessage(),
			        MessageCatalog.EDITOR_DOCUMENT_EMPTY.getMessage(), JOptionPane.INFORMATION_MESSAGE);
			log.logMessage(MessageCatalog.EDITOR_DOCUMENT_EMPTY.getMessage());
		}

		return retValue;
	}

	/**
	 * Searches in document the entered term, starting from cursor position.
	 */
	private void find() {

		if (!documentHandle.isEmpty()) {

			var dialog = new SearchAndReplaceDialogue(mainWindow, false);
			dialog.setVisible(true);
			if (!dialog.isCanceled()) {

				lastSearchTerm = dialog.getSearchTerm();
				findIsCaseSensitive = dialog.isCaseSensitive();
				findNext(true);
			}

		} else {

			JOptionPane.showMessageDialog(mainWindow, MessageCatalog.EDITOR_DOCUMENT_EMPTY.getMessage(),
			        MessageCatalog.EDITOR_DOCUMENT_EMPTY.getMessage(), JOptionPane.INFORMATION_MESSAGE);
			log.logMessage(MessageCatalog.EDITOR_DOCUMENT_EMPTY.getMessage());
		}
	}

	/**
	 * Replaces text according to values entered by user on dialogue.
	 */
	private void replace() {

		if (!documentHandle.isEmpty()) {

			var dialogue = new SearchAndReplaceDialogue(mainWindow, true);
			dialogue.setVisible(true);
			if (!dialogue.isCanceled()) {

				lastSearchTerm = dialogue.getSearchTerm();
				var replace = dialogue.getReplaceTerm();
				findIsCaseSensitive = dialogue.isCaseSensitive();
				var replaceAll = dialogue.isReplaceAll();
				if (lastSearchTerm.equals(replace)
				        || !findIsCaseSensitive && lastSearchTerm.equalsIgnoreCase(replace)) {

					JOptionPane.showMessageDialog(mainWindow, MessageCatalog.EDITOR_REPLACE_THE_SAME.getMessage(),
					        MessageCatalog.EDITOR_REPLACE_THE_SAME_NOTHING_TO_REPLACE.getMessage(),
					        JOptionPane.INFORMATION_MESSAGE);

				} else {

					var ocurrences = 0;
					if (replaceAll) {

						documentHandle.setCursorToBeginning();
						while (findNext(ocurrences == 0)) {

							documentHandle.replace(replace);
							ocurrences++;
						}

					} else if (findNext(ocurrences == 0)) {

						documentHandle.replace(replace);
						ocurrences++;
					}

					if (ocurrences > 0) {
						JOptionPane.showMessageDialog(mainWindow,
						        MessageCatalog.EDITOR_REPLACE_NUM_REPLACEMENTS.getMessage(ocurrences),
						        es.ree.eemws.kit.gui.common.MessageCatalog.MSG_INFO_TITLE.getMessage(),
						        JOptionPane.INFORMATION_MESSAGE);
					}
				}
			}

		} else {

			JOptionPane.showMessageDialog(mainWindow, MessageCatalog.EDITOR_DOCUMENT_EMPTY.getMessage(),
			        MessageCatalog.EDITOR_DOCUMENT_EMPTY.getMessage(), JOptionPane.INFORMATION_MESSAGE);
			log.logMessage(MessageCatalog.EDITOR_DOCUMENT_EMPTY.getMessage());
		}
	}

	/**
	 * Skips to the line entered on dialogue.
	 */
	private void goToLine() {

		if (!documentHandle.isEmpty()) {

			var lines = documentHandle.getPlainText().split("\n"); //$NON-NLS-1$
			var numberOfLines = lines.length;
			var loop = true;
			while (loop) {

				var lineNumber = JOptionPane.showInputDialog(mainWindow,
				        MessageCatalog.EDITOR_GO_TO_LINE_NUMBER.getMessage(numberOfLines),
				        es.ree.eemws.kit.gui.common.MessageCatalog.MSG_QUESTION_TITLE.getMessage(),
				        JOptionPane.QUESTION_MESSAGE);

				if (lineNumber != null) {

					try {

						var numLine = Integer.parseInt(lineNumber);
						if (numLine > 0 && numLine <= numberOfLines) {

							loop = false;

							var start = 0;
							for (var cont = 0; cont < numLine - 1; cont++) {

								start += lines[cont].length() + 1;
							}
							var end = start + lines[numLine - 1].length();
							documentHandle.markText(start, end);
						}

					} catch (NumberFormatException e) {

						/* Non numeric value entered. */
						loop = true;
					}

				} else {

					/* Cancel */
					loop = false;
				}
			}

		} else {

			JOptionPane.showMessageDialog(mainWindow, MessageCatalog.EDITOR_DOCUMENT_EMPTY.getMessage(),
			        MessageCatalog.EDITOR_DOCUMENT_EMPTY.getMessage(), JOptionPane.INFORMATION_MESSAGE);

		}
	}

	/**
	 * Text replace dialogue.
	 */
	class SearchAndReplaceDialogue extends JDialog {

		/** Class ID. */
		private static final long serialVersionUID = -8621170710549253308L;

		/** Search term text box. */
		private JTextField searchTxt;

		/** Replace by Text box. */
		private JTextField replaceTxt;

		/** Checkbox to indicate if all elements must be replaced. */
		private JCheckBox replaceAll;

		/**
		 * Checkbox to indicate whether the search must ignore capitalization.
		 */
		private JCheckBox caseSensitive;

		/** Indicates if search was cancelled. */
		private boolean isCanceled;

		/**
		 * Dialogue with search and replace parameters.
		 *
		 * @param window    Main window.
		 * @param isReplace toggle between search and replace modes.
		 */
		SearchAndReplaceDialogue(final Frame window, final boolean isReplace) {

			super(window);
			setModal(true);
			isCanceled = false;
			searchTxt = new JTextField(""); //$NON-NLS-1$
			searchTxt.setBounds(new Rectangle(90, 10, 190, 20));
			searchTxt.addActionListener(e -> accept());

			var searchLbl = new JLabel(MessageCatalog.EDITOR_FIND_LBL.getMessage());
			searchLbl.setLabelFor(searchTxt);
			searchLbl.setBackground(Color.RED);
			searchLbl.setDisplayedMnemonic(MessageCatalog.EDITOR_FIND_LBL_HK.getChar());
			searchLbl.setBounds(new Rectangle(10, 10, 50, 20));
			replaceTxt = new JTextField(""); //$NON-NLS-1$
			replaceTxt.setBounds(new Rectangle(90, 33, 190, 20));
			replaceTxt.setEnabled(isReplace);
			replaceTxt.addActionListener(e -> accept());

			var replaceLbl = new JLabel(MessageCatalog.EDITOR_REPLACE_LBL.getMessage());
			replaceLbl.setLabelFor(replaceTxt);
			replaceLbl.setDisplayedMnemonic(MessageCatalog.EDITOR_REPLACE_LBL_HK.getChar());
			replaceLbl.setBounds(new Rectangle(10, 33, 100, 20));
			replaceLbl.setEnabled(isReplace);
			replaceAll = new JCheckBox();
			replaceAll.setText(MessageCatalog.EDITOR_REPLACE_ALL_LBL.getMessage());
			replaceAll.setMnemonic(MessageCatalog.EDITOR_REPLACE_ALL_LBL_HK.getChar());
			replaceAll.setSelected(false);
			replaceAll.setBounds(new Rectangle(10, 66, 120, 20));
			replaceAll.setEnabled(isReplace);
			caseSensitive = new JCheckBox();
			caseSensitive.setText(MessageCatalog.EDITOR_REPLACE_CASE_SENSITIVE.getMessage());
			caseSensitive.setMnemonic(MessageCatalog.EDITOR_REPLACE_CASE_SENSITIVE_HK.getChar());
			caseSensitive.setSelected(true);
			caseSensitive.setSelected(false);
			caseSensitive.setBounds(new Rectangle(10, 90, 270, 20));
			var findButton = new JButton();
			findButton.setMaximumSize(new Dimension(100, 26));
			findButton.setMinimumSize(new Dimension(100, 26));
			findButton.setPreferredSize(new Dimension(100, 26));
			findButton.addActionListener(e -> accept());

			var separator = new JLabel();
			separator.setText("          "); //$NON-NLS-1$
			var cancelBtn = new JButton();
			cancelBtn.setMaximumSize(new Dimension(100, 26));
			cancelBtn.setMinimumSize(new Dimension(100, 26));
			cancelBtn.setPreferredSize(new Dimension(100, 26));
			cancelBtn.setMnemonic(MessageCatalog.EDITOR_CANCEL_BUTTON_HK.getChar());
			cancelBtn.setText(MessageCatalog.EDITOR_CANCEL_BUTTON.getMessage());
			cancelBtn.addActionListener(e -> cancel());

			var buttonLabel = new JPanel();
			buttonLabel.add(findButton, null);
			buttonLabel.add(separator, null);
			buttonLabel.add(cancelBtn, null);
			var textPanel = new JPanel();
			textPanel.setLayout(null);
			textPanel.add(searchLbl, null);
			textPanel.add(searchTxt, null);
			textPanel.add(replaceLbl, null);
			textPanel.add(replaceTxt, null);
			textPanel.add(replaceAll, null);
			textPanel.add(caseSensitive, null);
			getContentPane().setLayout(new BorderLayout());
			getContentPane().add(textPanel, BorderLayout.CENTER);
			getContentPane().add(buttonLabel, BorderLayout.SOUTH);
			setSize(new Dimension(300, 190));
			setResizable(false);
			if (isReplace) {
				setTitle(MessageCatalog.EDITOR_SEARCH_AND_REPLACE.getMessage());
				findButton.setMnemonic(MessageCatalog.EDITOR_SEARCH_AND_REPLACE_HK.getChar());
				findButton.setText(MessageCatalog.EDITOR_SEARCH_AND_REPLACE.getMessage());
			} else {
				setTitle(MessageCatalog.EDITOR_FIND.getMessage());
				findButton.setMnemonic(MessageCatalog.EDITOR_FIND_HK.getChar());
				findButton.setText(MessageCatalog.EDITOR_FIND.getMessage());
			}
			var screen = new Dimension(Toolkit.getDefaultToolkit().getScreenSize());
			setLocation(screen.width / 3, screen.height / 3);
			setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
			addWindowListener(new WindowAdapter() {
				@Override
				public void windowClosing(final WindowEvent e) { // NOSONAR event is not used.
					cancel();
				}
			});
		}

		/**
		 * Close Search dialogue.
		 */
		private void accept() {

			isCanceled = false;
			setVisible(false);
		}

		/**
		 * Close dialogue without making changes.
		 */
		private void cancel() {

			isCanceled = true;
			setVisible(false);
		}

		/**
		 * Indicate whether the search was cancelled.
		 *
		 * @return <code>true</code> if cancelled, <code>false</code> otherwise.
		 */
		public boolean isCanceled() {

			return isCanceled;
		}

		/**
		 * Indicate whether the capitalization must be ignored.
		 *
		 * @return <code>true</code> If capitalization is ignored<code>false</code>
		 *         otherwise.
		 */
		public boolean isCaseSensitive() {

			return caseSensitive.isSelected();
		}

		/**
		 * Indicate if all elements must be replaced.
		 *
		 * @return <code>true</code> replace all<code>false</code> otherwise.
		 */
		public boolean isReplaceAll() {

			return replaceAll.isSelected();
		}

		/**
		 * Retrieve search term entered by user.
		 *
		 * @return search term entered by user.
		 */
		public String getSearchTerm() {

			return searchTxt.getText();
		}

		/**
		 * Retrieve replace term entered by user.
		 *
		 * @return Replace term .
		 */
		public String getReplaceTerm() {

			return replaceTxt.getText();
		}
	}
}
