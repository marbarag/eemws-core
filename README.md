# IEC 62325-504 core #

[![eemws-core](https://maven-badges.herokuapp.com/maven-central/es.ree.eemws/eemws-core/badge.svg?style=flat&subject=eemws-core)](https://maven-badges.herokuapp.com/maven-central/es.ree.eemws/eemws-core) [![eemws-utils](https://maven-badges.herokuapp.com/maven-central/es.ree.eemws/eemws-utils/badge.svg?style=flat&subject=eemws-utils)](https://maven-badges.herokuapp.com/maven-central/es.ree.eemws/eemws-utils) [![eemws-client](https://maven-badges.herokuapp.com/maven-central/es.ree.eemws/eemws-client/badge.svg?style=flat&subject=eemws-client)](https://maven-badges.herokuapp.com/maven-central/es.ree.eemws/eemws-client) [![eemws-kit](https://maven-badges.herokuapp.com/maven-central/es.ree.eemws/eemws-kit/badge.svg?style=flat&subject=eemws-kit)](https://maven-badges.herokuapp.com/maven-central/es.ree.eemws/eemws-kit)

### What is this repository for? ###

This is a core and client implementation of IEC 62325-504 technical specification.

* **eemws-core** includes schemas, wsdl, and compiled classes necessary for the eem web services
* **eemws-utils** includes several useful classes to manage xml messages and their digital signatures
* **eemws-client** includes client classes to invoke the eem web services
* **eemws-kit** includes command line utilities to invoke the eem web services, as well as several GUI applications (browser, editor, ...).

Please use `./gradlew install` and java 11.x in order to compile.

Libraries are available in [Maven Central](https://search.maven.org/#search%7Cga%7C1%7Cg%3A%22es.ree.eemws%22)

App installer is available in the [download section.](https://bitbucket.org/smree/eemws-core/downloads/)

----

**News**

* **18-06-2024**: Version 2.1.3 available. Fixed a bug in the XmlUtil class. When an XML document contains a close character (">") within the value of an attribute, the returned XML was not valid i.e. \<tag v="one thing > other thing" />
* **17-06-2024**: Version 2.1.2 available. The libraries can now be built with java 11+ (tested with java 11 and java 21).
* **15-06-2024**: Version 2.1.1 available. Minor bug fixes. Don't use maven to build this library due to maven cxf-codegen-plugin not supporting the necessary binding extension. **WARNING!!** If you use `X509Util.checkCertificate()` directly, no Exceptions will be thrown now; you need to explicitly verify the result of the check.
* **29-03-2024**: Version 2.1.0 available. Java 11 compatible using the newest "jakarta" dependencies. Tested with Java 11 and Java 21. New feature environment files (read the manual!). Bug Fixed: command line for get operation always returned the pretty print version of the document without taking into account the "USE_PRETTY_PRINT_OUTPUT" flag. Some code improvements. **WARNING!!** old encripted passwords are not compatible with this new version. When  upgrading, you should write passwords in clear text before running the program.
* **18-04-2023**: Version 2.0.0 available. Java 11 compatible version, still using old "javax" dependencies (for java 8 keep using version 1.x).
* **10-04-2023**: Version 1.3.5 available. Fixed bug related to daylight saving time reported by RTE.
* **22-06-2022**: New version of the IEC 62325-504 Test Cases document. We have added a new XML namespace consideration and some other minor corrections.
* **24-08-2021**: Version 2.0.1-RC1 available. Java 11 compatible version using the new "jakarta" dependencies.
* **07-04-2021**: Version 1.3.4 available. Fixed UTF-8 enconding problem in properties files.
* **20-12-2020**: Version 1.3.2 available. Magic-folder bug fix: Saving the latest message code feature now is aware of the use of several instances. Added a new parameter in Magic Folder to retry failed "get" operations
* **21-01-2020**: Version 1.3.1 available. Magic-folder bug fix: Null reference was used when saving "Ok" responses and the ACK_FOLDER_OK setting is set.
* **08-01-2020**: Version 1.3 available. Magic-folder improvements (eemws-kit): reset message code possible, multiple id-patterns for downloadable messages possible. Core libraries equivalent to version 1.2.2
* **13-06-2019**: Version 1.2.2 available. Some bugs regarding "compress" data were fixed.
* **08-01-2018**: Version 1.2.1 available. Core web service client rewritten in order to avoid signature validation bug ([JDK-8186441](https://bugs.openjdk.java.net/browse/JDK-8186441)). Magic folder can now send binary files.
* **29-08-2017**: Version 1.2.0 available. Bumped gradle version. Java 8 required. Libraries are equivalent to version 1.1.4
* **28-08-2017**: **WARNING!!**: A bug ([JDK-8186441](https://bugs.openjdk.java.net/browse/JDK-8186441)) intruduced in the latest Java 1.8 versions (8u141, 8u144) produces bad behaviour in the signature process (create signature / verifity signature). We advise you not to use such java versions.
* **25-08-2017**: Version 1.1.4 available. Fixes invalid path in schema path.
* **04-11-2016**: Version 1.1.3 available. New Magic Folder features and small fixes.
* **16-07-2016**: Fixed a bug in Windows service wrapper distribution installer.
* **06-07-2016**: Version 1.1.2 available. Javadoc corrections, xsd and wsdl location fixes within the jar file. Thanks Emanuel for the contribution! 
* **29-06-2016**: Version 1.1.0 available. Libraries published on Maven Central! Please, note the change in the groupId coordinates.
* **08-06-2016**: Version 1.0.1 available. Small fix to gitignore.
* **02-06-2016**: **Version 1.0.0!** Now compiled with gradle (no need to install it, just use the included wrapper)
* **18-04-2016**: New connection kit version (1.0-m12). Magic Folder now can deal with several servers for sending / receiving messages. Added a new program for sending xml documents or binary files using a simple graphical application.
* **04-05-2016**: Windows service wrapper (by Tanuki Software) updated to version 3.5.29
* **01-04-2016**: New connection kit version (1.0-m11) available. Magic Folder now can deal with several input / output folders and can execute scripts / programs as well.
* **28-03-2016**: Check any IEC 62325-504 implementation with [this SoapUI project](https://bitbucket.org/smree/eemws-core/downloads/IEC-62535-504-soapui-project.xml) and [this document guide!](https://bitbucket.org/smree/eemws-core/downloads/IEC%2062325-504%20Test%20Cases.pdf)
* **25-03-2016**: New document explaining [security aspects](https://bitbucket.org/smree/eemws-core/downloads/Understanding%20security%20in%20communications%20with%20IEC%2062325-504.pdf)
* **24-03-2016**: New user's manual version (1.3) [available](https://bitbucket.org/smree/eemws-core/downloads/Connection%20Kit%20User's%20manual%20v.1.3.pdf)
* **20-11-2015**: New connection kit version (1.0-m10) with small fixes. Make it easier to use compressed payloads. Various Magic Folder fixes
* **20-11-2015**: New user's manual version (1.2)
* **21-10-2015**: New connection kit version (1.0-m9) with small fixes and use of SHA-256 for digest and signing messages instead of deprecated SHA-1 available (see Downloads section).
* **14-10-2015**: User's manual review available.
* **29-07-2015**: New connection kit version (1.0-m8) with small fixes.
* **03-06-2015**: IEC 62325-504 is now available!! (https://twitter.com/iec_csc/status/606061621010694146)
* **12-05-2015**: Added an utility pack to use Magic Folder as a Windows Service see Downloads section.
* **18-03-2015**: New connection kit version (1.0-m7) with binary support available (see Downloads section).
* **29-01-2015**: First user's manual version (no draft) available (see Downloads section)
* **29-01-2015**: New connection kit version (1.0-m6) with improvements and fixes available (see Downloads section). The application is now available in Spanish and English.
* **17-11-2014**: The first draft of the user's manual is available

### Who do I talk to? ###

* If you have questions please contact soportesios@ree.es

### How do I compile the project? ###

As stated above, you simply need to use the gradle wrapper included in the root of the repository:

```shell
$ ./gradlew build
```

This will take care of downloading gradle itself and all the project's dependencies.

<!-- The `install` task is similar to maven's one. The project artifacts will be installed in your local Maven repository. -->

The version of the project will be calculated automatically depending on the state of the repository and the working area.

* If there is a tag in your current commit, that tag will be used as version base
* If there isn't a tag in your current commit, the nearest tag (git describe), automatically incremented, will be used as version base
* If there isn't a tag in your current commit or the working dir is dirty, `-SNAPSHOT` will be added to the version
* The auto-increment will use next-minor-version for all branches except `master` and `release/x.y.z`; for these last two, next-patch-version will be used.

For more information, see the [build file](./build.gradle) and the [axion-release-plugin](https://github.com/allegro/axion-release-plugin).

### How to compile version x.y.z ###

In order to compile a specific version, you may checkout said version and compile with gradle:

```shell
$ git checkout x.y.z
$ ./gradlew build
```

If in the previous case you get a `-SNAPSHOT` version due to a dirty repository, but you really want to build the version without the snapshot indicator, first check that the modifications in the repository are correct and then you may use a specific property to ignore uncommited changes when generating the version:

```shell
$ ./gradlew build -Prelease.ignoreUncommittedChanges=true
```
```
