/*
 * Copyright 2024 Redeia.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTIBIILTY or FITNESS FOR A PARTICULAR PURPOSE. See GNU Lesser General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see
 * http://www.gnu.org/licenses/.
 *
 * Any redistribution and/or modification of this program has to make
 * reference to Redeia as the copyright owner of the program.
 */

package es.ree.eemws.kit.gui.applications.browser;

import es.ree.eemws.kit.common.Messages;

/**
 * Message codes for the Kit's settings.
 *
 * @author Redeia.
 * @version 2.1 01/01/2024
 */
public enum MessageCatalog {

	/** Code. */
	BROWSER_COLUMN_CODE,

	/** ID. */
	BROWSER_COLUMN_ID,

	/** Version. */
	BROWSER_COLUMN_VERSION,

	/** Status. */
	BROWSER_COLUMN_STATUS,

	/** Appl. Start Time. */
	BROWSER_COLUMN_APPLICATION_ST_TIME,

	/** Appl. End Time. */
	BROWSER_COLUMN_APPLICATION_END_TIME,

	/** Server Timestamp. */
	BROWSER_COLUMN_SERVER_TIMESTAMP,

	/** Msg. Type. */
	BROWSER_COLUMN_MSG_TYPE,

	/** Owner. */
	BROWSER_COLUMN_OWNER,

	/** Simple. */
	BROWSER_SIMPLE_VIEW,

	/** Simple hot key. */
	BROWSER_SIMPLE_VIEW_HK,

	/** Full, */
	BROWSER_FULL_VIEW,

	/** Full hot key. */
	BROWSER_FULL_VIEW_HK,

	/** Columns. */
	BROWSER_COLUMN_MENU_ENTRY,

	/** Columns hot key. */
	BROWSER_COLUMN_MENU_ENTRY_HK,

	/** Select. */
	BROWSER_SELECT_MENU_ENTRY,

	/** Select hot key. */
	BROWSER_SELECT_MENU_ENTRY_HK,

	/** Select All. */
	BROWSER_SELECT_ALL_MENU_ENTRY,

	/** Select All hot key. */
	BROWSER_SELECT_ALL_MENU_ENTRY_HK,

	/** Clear selection. */
	BROWSER_SELECT_NONE_MENU_ENTRY,

	/** Clear selection hot key. */
	BROWSER_SELECT_NONE_MENU_ENTRY_HK,

	/** Invert selection. */
	BROWSER_SELECT_INVERT_MENU_ENTRY,

	/** Invert selection hot key. */
	BROWSER_SELECT_INVERT_MENU_ENTRY_HK,

	/** 1 message. */
	BROWSER_STATUS_MESSAGE,

	/** {0} messages. */
	BROWSER_STATUS_MESSAGES,

	/** {0} messages match filter criteria. */
	BROWSER_STATUS_MESSAGES_RETRIEVED,

	/** No message matches filter criteria. */
	BROWSER_STATUS_NO_MESSAGES_RETRIEVED,

	/** 1 message (1 selected) */
	BROWSER_STATUS_MESSAGE_SELECTED,

	/** {0} messages ({1} selected) */
	BROWSER_STATUS_MESSAGES_SELECTED,

	/** Create backup */
	BROWSER_FILE_BACKUP_MENU_ENTRY,

	/** Create backup hot key. */
	BROWSER_FILE_BACKUP_MENU_ENTRY_HK,

	/** Set default folder */
	BROWSER_FILE_SET_FOLDER_MENU_ENTRY,

	/** Defalult forlder hot key. */
	BROWSER_FILE_SET_FOLDER_MENU_ENTRY_HK,

	/** Exit. */
	BROWSER_FILE_EXIT_MENU_ENTRY,

	/** Exit hot key. */
	BROWSER_FILE_EXIT_MENU_ENTRY_HK,

	/** File. */
	BROWSER_FILE_MENU_ENTRY,

	/** File hot key. */
	BROWSER_FILE_MENU_ENTRY_HK,

	/** Backup created {0} */
	BROWSER_FILE_BACKUP_CREATED,

	/** Replace already existing file {0}? */
	BROWSER_FILE_REPLACE_FILE,

	/** Replace existing file. */
	BROWSER_FILE_REPLACE_FILE_TITLE,

	/** File {0} won't be replaced. Message was not saved. */
	BROWSER_FILE_NO_REPLACE,

	/** File {0} saved. */
	BROWSER_FILE_FILE_SAVED,

	/** Exit application? */
	BROWSER_FILE_EXIT_APPLICATION,

	/** Exit? */
	BROWSER_FILE_EXIT_APPLICATION_TITLE,

	/** Type of filter: */
	BROWSER_FILTER_TYPE,

	/** Type of filter hot key. */
	BROWSER_FILTER_TYPE_HK,

	/** Code. */
	BROWSER_FILTER_TYPE_CODE,

	/** Server timestamp. */
	BROWSER_FILTER_TYPE_SERVER,

	/** Application date. */
	BROWSER_FILTER_TYPE_APPLICATION,

	/** Start date: */
	BROWSER_FILTER_START_DATE,

	/** Start date hot key. */
	BROWSER_FILTER_START_DATE_HK,

	/** End date: */
	BROWSER_FILTER_END_DATE,

	/** End date hot key. */
	BROWSER_FILTER_END_DATE_HK,

	/** Invalid date format. {0}, /** {1} must be formatted as "{2}" */
	BROWSER_FILTER_INCORRECT_DATE_FORMAT,

	/** Code: */
	BROWSER_FILTER_CODE,

	/** Incorrect code. "{0}" must be a positive number. */
	BROWSER_FILETER_INCORRECT_CODE,

	/** Code hot key. */
	BROWSER_FILTER_CODE_HK,

	/** Id: */
	BROWSER_FILTER_ID,

	/** Filter id hot key. */
	BROWSER_FILTER_ID_HK,

	/** Msg type: */
	BROWSER_FILTER_MSG_TYPE,

	/** Msg type hot key. */
	BROWSER_FILTER_MSG_TYPE_HK,

	/** Owner: */
	BROWSER_FILTER_OWNER,

	/** Owener hot key. */
	BROWSER_FILTER_OWNER_HK,

	/** List */
	BROWSER_FILTER_BROWSER_BUTTON,

	/** List hot key. */
	BROWSER_FILTER_BROWSER_BUTTON_HK,

	/** Get */
	BROWSER_FILTER_GET_BUTTON,

	/** Get hot key. */
	BROWSER_FILTER_GET_BUTTON_HK,

	/** List filters */
	BROWSER_FILTER_LEGEND,

	/** Show filters */
	BROWSER_FILTER_SHOW_FILTER_MENU_ENTRY,

	/** Show hot key. */
	BROWSER_FILTER_SHOW_FILTER_MENU_ENTRY_HK,

	/** Browser. */
	BROWSER_MAIN_WINDOW_TITLE,

	/** View. */
	BROWSER_VIEW_MENU_ITEM,

	/** View hot key. */
	BROWSER_VIEW_MENU_ITEM_HK,

	/** Ready */
	BROWSER_STATUS_READY,

	/** Ok */
	BROWSER_STATUS_OK,

	/** Failed. */
	BROWSER_STATUS_FAILED,

	/** Check filter values. \n {0} */
	BROWSER_CHECK_FILTER_ERROR_MSG,

	/** No messages. */
	BROWSER_NO_MESSAGES_TITLE,

	/** Unable to invoke "list" operation: {0} */
	BROWSER_UNABLE_TO_LIST,

	/** Unable to invoke "get" operation: {0} */
	BROWSER_UNABLE_TO_GET,

	/** Unexpected error invoking "list" operation. Check log details. */
	BROWSER_UNABLE_TO_BROWSER_UNKNOW,

	/** Retrieving message {0} with code {1} */
	BROWSER_RETRIEVING_FILE,

	/** Retrieved message {0} with code {1} */
	BROWSER_RETRIEVED_FILE,

	/** There aren't messages to retrieve. */
	BROWSER_NO_MESSAGES_TO_GET,

	/** Select the message to retrieve. */
	BROWSER_SELECT_MESSAGES_TO_GET,

	/** Retrieving {0} messages. */
	BROWSER_RETRIEVING_SEVERAL_MESSAGES,

	/** Retrieve message {0} with code {1}? */
	BROWSER_RETRIEVE_MESSAGE_CONFIRMATION;

	/**
	 * Gets message text.
	 *
	 * @return Message text.
	 */
	public String getMessage(final Object... parameters) {
		return Messages.getString(name(), parameters);
	}

	/**
	 * Return the first chart of the message.
	 *
	 * @return First chart of the message.
	 */
	public char getChar() {
		return Messages.getString(name()).charAt(0);
	}
}
