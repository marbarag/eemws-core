/*
 * Copyright 2024 Redeia.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTIBIILTY or FITNESS FOR A PARTICULAR PURPOSE. See GNU Lesser General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see
 * http://www.gnu.org/licenses/.
 *
 * Any redistribution and/or modification of this program has to make
 * reference to Redeia as the copyright owner of the program.
 */

package es.ree.eemws.kit.gui.applications.editor;

import java.awt.Component;
import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

import es.ree.eemws.client.put.PutMessage;
import es.ree.eemws.core.utils.config.ConfigException;
import es.ree.eemws.core.utils.iec61968100.EnumMessageStatus;
import es.ree.eemws.core.utils.operations.put.PutOperationException;
import es.ree.eemws.kit.config.Configuration;
import es.ree.eemws.kit.gui.common.Logger;
import es.ree.eemws.kit.gui.common.ServiceMenuListener;

/**
 * Processes related to message send.
 *
 * @author Redeia.
 * @version 2.1 01/01/2024
 */
public final class SendHandle implements ServiceMenuListener {

	/** Access to log window. */
	private Logger log;

	/** Button bar. */
	private JToolBar buttonBar = new JToolBar();

	/** Send menu. */
	private JMenu serviceMenu = new JMenu();

	/** Main Window. */
	private Editor mainWindow = null;

	/** Editable text manager. */
	private DocumentHandle documentHandle = null;

	/**
	 * Constructor. Creates a new private instance of the Send handler class.
	 *
	 * @param window Main window.
	 * @throws ConfigException If module settings are incorrect.
	 */
	public SendHandle(final Editor window) throws ConfigException {

		var cf = new Configuration();
		cf.readConfiguration();

		mainWindow = window;
		log = mainWindow.getLogHandle().getLog();
		documentHandle = mainWindow.getDocumentHandle();
	}

	/**
	 * Obtains the service menu items.
	 *
	 * @return Menu entry containing 'Settings' menu items.
	 */
	public JMenu getMenu() {

		var sendMenuItem = new JMenuItem(MessageCatalog.EDITOR_MENU_ITEM_SEND.getMessage(),
		        new ImageIcon(getClass().getResource(es.ree.eemws.kit.gui.common.Constants.ICON_SEND)));
		sendMenuItem.setMnemonic(MessageCatalog.EDITOR_MENU_ITEM_SEND_HK.getChar());
		sendMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F9, 0));
		sendMenuItem.addActionListener(e -> send());

		serviceMenu.setText(MessageCatalog.EDITOR_MENU_ITEM_SERVICE.getMessage());
		serviceMenu.setMnemonic(MessageCatalog.EDITOR_MENU_ITEM_SERVICE_HK.getChar());
		serviceMenu.add(sendMenuItem);
		serviceMenu.addSeparator();

		return serviceMenu;
	}

	/**
	 * Returns the sending option bar.
	 *
	 * @return Sending option bar.
	 */
	public JToolBar getButtonBar() {

		buttonBar = new JToolBar();
		buttonBar.setFloatable(true);

		var enviarDocBtn = new JButton();
		enviarDocBtn.setToolTipText(MessageCatalog.EDITOR_MENU_ITEM_SEND.getMessage());
		enviarDocBtn.setIcon(new ImageIcon(getClass().getResource(es.ree.eemws.kit.gui.common.Constants.ICON_SEND)));
		enviarDocBtn.setBorderPainted(false);
		enviarDocBtn.addActionListener(e -> send());

		buttonBar.add(enviarDocBtn, null);
		return buttonBar;
	}

	/**
	 * Enables / disables graphic values.
	 *
	 * @param activeValue <code>true</code> enable. <code>false</code> disable.
	 */
	public void enable(final boolean activeValue) {

		for (Component comp : buttonBar.getComponents()) {
			comp.setEnabled(activeValue);
		}

		for (Component comp : serviceMenu.getMenuComponents()) {
			comp.setEnabled(activeValue);
		}
	}

	/**
	 * Sends XML files.
	 */
	private void send() {

		if (documentHandle.isEmpty()) {

			log.logMessage(MessageCatalog.EDITOR_SEND_DOCUMENT_IS_EMPTY.getMessage());
			JOptionPane.showMessageDialog(mainWindow, MessageCatalog.EDITOR_SEND_DOCUMENT_IS_EMPTY.getMessage(),
			        es.ree.eemws.kit.gui.common.MessageCatalog.MSG_INFO_TITLE.getMessage(),
			        JOptionPane.INFORMATION_MESSAGE);

		} else {

			mainWindow.enableScreen(false);
			SwingUtilities.invokeLater(this::sendDataAfterDisableComponents);
		}
	}

	/**
	 * Sends the current message.
	 */
	private void sendDataAfterDisableComponents() {

		try {

			var put = new PutMessage();
			var config = new Configuration();
			config.readConfiguration();
			put.setEndPoint(config.getUrlEndPoint());

			log.logMessage(MessageCatalog.EDITOR_SENDING.getMessage());
			var l = System.currentTimeMillis();

			var response = put.put(new StringBuilder(documentHandle.getPlainText()));

			l = (System.currentTimeMillis() - l) / 1000;

			log.logMessage(MessageCatalog.EDITOR_ACK_RECEIVED.getMessage());
			if (response != null) {
				log.logMessage(response);
			}

			var status = put.getMessageMetaData().getStatus();

			if (status == null) {
				JOptionPane.showMessageDialog(mainWindow, MessageCatalog.EDITOR_NO_IEC_MESSAGE.getMessage(),
				        es.ree.eemws.kit.gui.common.MessageCatalog.MSG_ERROR_TITLE.getMessage(),
				        JOptionPane.INFORMATION_MESSAGE);

			} else if (status.equals(EnumMessageStatus.OK)) {
				JOptionPane.showMessageDialog(mainWindow, MessageCatalog.EDITOR_ACK_OK.getMessage(l),
				        es.ree.eemws.kit.gui.common.MessageCatalog.MSG_INFO_TITLE.getMessage(),
				        JOptionPane.INFORMATION_MESSAGE);
			} else {
				JOptionPane.showMessageDialog(mainWindow, MessageCatalog.EDITOR_ACK_NOOK.getMessage(),
				        es.ree.eemws.kit.gui.common.MessageCatalog.MSG_WARNING_TITLE.getMessage(),
				        JOptionPane.INFORMATION_MESSAGE);
			}

		} catch (PutOperationException ex) {

			var msg = MessageCatalog.EDITOR_UNABLE_TO_SEND.getMessage();
			JOptionPane.showMessageDialog(mainWindow, msg,
			        es.ree.eemws.kit.gui.common.MessageCatalog.MSG_ERROR_TITLE.getMessage(), JOptionPane.ERROR_MESSAGE);
			log.logException(msg, ex);

		} catch (ConfigException ex) {

			var msg = es.ree.eemws.kit.cmd.MessageCatalog.INVALID_CONFIGURATION.getMessage(ex.getMessage());
			JOptionPane.showMessageDialog(mainWindow, msg,
			        es.ree.eemws.kit.gui.common.MessageCatalog.MSG_ERROR_TITLE.getMessage(), JOptionPane.ERROR_MESSAGE);
			log.logException(msg, ex);

		} finally {

			mainWindow.enableScreen(true);
		}
	}

	/**
	 * Sets end point.
	 *
	 * @param endp End point to messages will be sent.
	 */
	@Override
	public void setEndPoint(final String endp) {

		/* This method should not be implemented. */
	}
}
