/*
 * Copyright 2024 Redeia.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTIBIILTY or FITNESS FOR A PARTICULAR PURPOSE. See GNU Lesser General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see
 * http://www.gnu.org/licenses/.
 *
 * Any redistribution and/or modification of this program has to make
 * reference to Redeia as the copyright owner of the program.
 */

package es.ree.eemws.kit.gui.applications.sender;

import es.ree.eemws.kit.common.Messages;

/**
 * Message catalog for message sender application.
 *
 * @author Redeia.
 * @version 2.1 01/01/2024
 */
public enum MessageCatalog {

	/** File sender. */
	SENDER_TITLE,

	/** Sending: {0} */
	SENDER_SENDING,

	/** File {0} was rejected. */
	SENDER_FILE_FAILED,

	/** File {0} was accepted. */
	SENDER_FILE_OK,

	/** Drag file here. */
	SENDER_DRAG_FILE_HERE,

	/** Open. */
	SENDER_MENU_ITEM_OPEN,

	/** Open hot key. */
	SENDER_MENU_ITEM_OPEN_HK,

	/** Send as binary. */
	SENDER_SEND_AS_BINARY,

	/** Send as binary hot key. */
	SENDER_SEND_AS_BINARY_HK,

	/** Exit. */
	SENDER_MENU_ITEM_EXIT,

	/** Exit hot key. */
	SENDER_MENU_ITEM_EXIT_HK,

	/** File. */
	SENDER_MENU_ITEM_FILE,

	/** File hot key. */
	SENDER_MENU_ITEM_FILE_HK,

	/** Server returns a no IEC-61968100 message. See log for details. */
	SENDER_NO_IEC_MESSAGE,

	/** Successfully sent in {0} second(s). */
	SENDER_ACK_OK,

	/** Server rejected the message. See log for details. */
	SENDER_ACK_NOOK,

	/** You can only drag files here no folders! */
	SENDER_CANNOT_LOAD_FOLDER,

	/** Cannot open {0}.\n Make sure file name is correct and you have read permission. */
	SENDER_CANNOT_OPEN_FILE,

	/** Unable to send the document!. See log for details. */
	SENDER_UNABLE_TO_SEND,

	/** Save. */
	SENDER_MENU_ITEM_SAVE,

	/** Save hot key. */
	SENDER_MENU_ITEM_SAVE_HK,

	/** Auto. */
	SENDER_SAVE_AUTO,

	/** Auto hot key. */
	SENDER_SAVE_AUTO_HK,

	/** Ask. */
	SENDER_ASK_SAVE,

	/** Ask hot key. */
	SENDER_ASK_SAVE_HK,

	/** Do not save. */
	SENDER_NO_SAVE,

	/** Do not save hot key. */
	SENDER_NO_SAVE_HK,

	/** Response saved in file {0} */
	SENDER_SAVE_FILE_SAVED,

	/** Unable to save response in file {0}. Check log for details. */
	SENDER_UNABLE_TO_SAVE,

	/** File {0} already exists. Do you want to overwrite it? */
	SENDER_SAVE_FILE_ALREADY_EXISTS,

	/** Server has sent an empty response. */
	SENDER_NO_RESPONSE,

	/** Cannot create fault from exception. Check stack trace for details. */
	SENDER_CANNOT_CREATE_FAULT_MSG;

	/**
	 * Gets message text.
	 * @return Message text.
	 */
	public String getMessage(final Object... parameters) {
		return Messages.getString(name(), parameters);
	}

	/**
	 * Return the first chart of the message.
	 * @return First chart of the message.
	 */
	public char getChar() {
		return Messages.getString(name()).charAt(0);
	}
}


