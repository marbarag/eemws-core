/*
 * Copyright 2024 Redeia.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTIBIILTY or FITNESS FOR A PARTICULAR PURPOSE. See GNU Lesser General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see
 * http://www.gnu.org/licenses/.
 *
 * Any redistribution and/or modification of this program has to make
 * reference to Redeia as the copyright owner of the program.
 */

package es.ree.eemws.core.utils.security;

import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.util.Date;

import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import es.ree.eemws.core.utils.i18n.MessageCatalog;

/**
 * Implements simple X509 validations and utilities.
 *
 * @author Redeia.
 * @version 2.1 01/01/2024
 */
public final class X509Util {

    /** TrustFactory algorimth names. */
    private static final String[] ALGORITHM_NAMES = { "SunX509", "IbmX509" }; //$NON-NLS-1$ //$NON-NLS-2$

    /** Authentication type. */
    private static final String AUTHENTICATION_TYPE = "RSA"; //$NON-NLS-1$

    /** Trust manager, validate that the given certificate was issued by a trusted CA. */
    private static final X509TrustManager X509_TRUST_MANAGER;

    /*
     * Initializes the x509 trust manager.
     */
    static {
        TrustManagerFactory tmf = null;

        for (int cont = 0; tmf == null && cont < ALGORITHM_NAMES.length; cont++) {
            try {
                tmf = TrustManagerFactory.getInstance(ALGORITHM_NAMES[cont]);
            } catch (@SuppressWarnings("unused") NoSuchAlgorithmException e) { //NOSONAR - We specifically do not want to propagate nor log this exception

                /* No algorithm[i] available, try next one... */
            }
        }

        if (tmf == null) {
            throw new IllegalStateException(MessageCatalog.SECURITY_NO_TRUST_VALIDATOR.getMessage());

        }

        try {
            tmf.init((KeyStore) null);
            X509_TRUST_MANAGER = (X509TrustManager) tmf.getTrustManagers()[0];
        } catch (KeyStoreException e) {

        	throw new IllegalStateException(MessageCatalog.SECURITY_UNABLE_TO_INITIALIZE_TRUST_VALIDATOR.getMessage(tmf.getAlgorithm()), e);
        }
    }

    /**
     * Constructor.
     */
    private X509Util() {

        /* This method should not be implemented. */
    }

    /**
     * Checks the given X509 certificate.
     * @param x509Cert Certificate to be validated.
     * @return A validation status class.
     */
    public static X509ValidationStatus checkCertificate(final X509Certificate x509Cert)  {

        return checkCertificate(new X509Certificate[] { x509Cert });
    }

    /**
     * Checks the given X509 certificates.
     * @param x509Certs Certificates to be validated.
     * @return A validation status class. 
     */
    public static X509ValidationStatus checkCertificate(final X509Certificate[] x509Certs)  {
    	
		var status = new X509ValidationStatus();
		X509Certificate currentCert = null;
		
    	try {

    		X509_TRUST_MANAGER.checkClientTrusted(x509Certs, AUTHENTICATION_TYPE);
			for (int cont = 0; cont < x509Certs.length; cont++) {
				currentCert = x509Certs[cont]; 
				currentCert.checkValidity();
			}

		} catch (CertificateNotYetValidException | CertificateExpiredException e) {
			
			status = new X509ValidationStatus(MessageCatalog.SECURITY_SIGNATURE_CERTIFICATE_NOT_VALID.getMessage(new Date(), currentCert.getNotBefore(), currentCert.getNotAfter()), e); 
			
		} catch (CertificateException e) {
			
			status = new X509ValidationStatus(MessageCatalog.SECURITY_SIGNATURE_NO_TRUSTED_CERT.getMessage(), e);
			
		}
    	
    	return status;
        
    }
    
    
}
