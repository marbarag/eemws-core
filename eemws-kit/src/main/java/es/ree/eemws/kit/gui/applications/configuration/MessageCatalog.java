/*
 * Copyright 2024 Redeia.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTIBIILTY or FITNESS FOR A PARTICULAR PURPOSE. See GNU Lesser General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see
 * http://www.gnu.org/licenses/.
 *
 * Any redistribution and/or modification of this program has to make
 * reference to Redeia as the copyright owner of the program.
 */

package es.ree.eemws.kit.gui.applications.configuration;

import es.ree.eemws.kit.common.Messages;

/**
 * Message codes for the Kit's settings.
 *
 * @author Redeia.
 * @version 2.1 01/01/2024
 */
public enum MessageCatalog {

	/** Cannot set native L&amp;F! */
	SETTINGS_NO_GUI,

	/** Error */
	SETTINGS_NO_SETTINGS_TITLE,

	/** Application miss configured. Run setup first. */
	SETTINGS_NO_CONFIGURATION,

	/** Error */
	SETTINGS_NO_CONFIGURATION_TITLE,

	/** Connection Kit settings */
	SETTINGS_TITLE,

	/** Ok */
	SETTINGS_OK_BUTTON,

	/** Ok hot key */
	SETTINGS_OK_BUTTON_HK,

	/** Saves configuration and exits. */
	SETTINGS_OK_BUTTON_TIP,

	/** Cancel */
	SETTINGS_CANCEL_BUTTON,

	/** Cancel hot key */
	SETTINGS_CANCEL_BUTTON_HK,

	/** Exit without saving. */
	SETTINGS_CANCEL_BUTTON_TIP,

	/** Do you want to exit and ignore changes? */
	SETTINGS_CANCEL_TEXT,

	/** Do you want to save changes and exit application? */
	SETTINGS_SAVE_CHANGES,

	/** Panel says: */
	SETTINGS_PANEL_SAYS,

	/** Unable store configuration. There are errors:\n */
	SETTINGS_CONFIG_HAS_ERRORS,

	/** Unable store configuration. There are errors:\n */
	SETTINGS_INVALID_ENVIRONMENT_FILE,

	/** Magic folder settings are incorrect. Do you want to save and exit anyway? */
	SETTINGS_CONFIG_MAGIC_FOLDER,

	/** System is misconfigured. Check program settings and restart. */
	SETTINGS_MISS_CONFIGURED,

	/**
	 * System configuration is invalid. The program will use a blank configuration.
	 */
	SETTINGS_NO_CONFIG,

	/** Identity */
	SETTINGS_IDENTITY_TAB,

	/** Certificate file: */
	SETTINGS_IDENTITY_CERTIFICATE_FILE,

	/** Certificate hot key */
	SETTINGS_IDENTITY_CERTIFICATE_FILE_HK,

	/** Password: */
	SETTINGS_IDENTITY_CERTIFICATE_PASSWORD,

	/** Password hot key */
	SETTINGS_IDENTITY_CERTIFICATE_PASSWORD_HK,

	/** Confirm password: */
	SETTINGS_IDENTITY_CERTIFICATE_PASSWORD2,

	/** Password hot key */
	SETTINGS_IDENTITY_CERTIFICATE_PASSWORD2_HK,

	/** Browse... */
	SETTINGS_IDENTITY_CERTIFICATE_BROWSE,

	/** Browse hot key */
	SETTINGS_IDENTITY_CERTIFICATE_BROWSE_HK,

	/** Enter certificate configuration */
	SETTINGS_IDENTITY_CERTIFICATE_DATA,

	/** Certificate file doesn't exists. */
	SETTINGS_IDENTITY_FILE_DOESNT_EXISTS,

	/** Must provide a path to a certificate file. */
	SETTINGS_IDENTITY_MUST_PROVIDE_CERTIFICATE_FILE,

	/** Unable to read the certificate file. */
	SETTINGS_IDENTITY_FILE_CANNOT_READ,

	/** Password and Confirm password do not match */
	SETTINGS_IDENTITY_PASSWORD_MATCH,

	/** Unable to read the certificate. Check certificate settings. */
	SETTINGS_IDENTITY_CERTIFICATE_CANNOT_BE_READ,

	/**
	 * There are no usable certificates in the keystore (Certificates are expired,
	 * or has no private key)
	 */
	SETTINGS_IDENTITY_NO_USABLE_CERTIFICATE,

	/** Proxy */
	SETTINGS_PROXY_TAB,

	/** Host: */
	SETTINGS_PROXY_HOST,

	/** Host hot key */
	SETTINGS_PROXY_HOST_HK,

	/** Port: */
	SETTINGS_PROXY_PORT,

	/** Port hot key */
	SETTINGS_PROXY_PORT_HK,

	/** Password: */
	SETTINGS_PROXY_PASSWORD,

	/** Password hot key */
	SETTINGS_PROXY_PASSWORD_HK,

	/** Confirm password: */
	SETTINGS_PROXY_PASSWORD2,

	/** Password hot key */
	SETTINGS_PROXY_PASSWORD2_HK,

	/** User: */
	SETTINGS_PROXY_USER,

	/** User hot key */
	SETTINGS_PROXY_USER_HK,

	/** Enter parameters */
	SETTINGS_PROXY_PARAMETERS,

	/** Use proxy */
	SETTINGS_PROXY_USE_PROXY,

	/** proxy hot key */
	SETTINGS_PROXY_USE_PROXY_HK,

	/** Direct Internet connection. */
	SETTINGS_PROXY_DIRECT_CONNECTION,

	/** Direct hot key */
	SETTINGS_PROXY_DIRECT_CONNECTION_HK,

	/** Enter proxy settings for your Internet connection: */
	SETTINGS_PROXY_SETTINGS,

	/** Must set Proxy host name or IP address */
	SETTINGS_PROXY_NO_HOST,

	/** Port must be a value between 1 and 65535 */
	SETTINGS_PROXY_INVALID_PORT,

	/** Listening port must be an integer value between 1 and 65535. */
	SETTINGS_PROXY_INVALID_PORT_NO_NUMERIC,

	/** Password and Confirm password do not match */
	SETTINGS_PROXY_PASSWORD_MATCH,

	/** If the proxy settings uses password, a user id must be also set. */
	SETTINGS_PROXY_NO_USER,

	/** Magic Folder */
	SETTINGS_FOLDER_TAB,

	/** Backup: */
	SETTINGS_FOLDER_BACKUP_FOLDER,

	/** Back hot key */
	SETTINGS_FOLDER_BACKUP_FOLDER_HK,

	/** Backup */
	SETTINGS_FOLDER_BACKUP_BORDER,

	/** Browse folder */
	SETTINGS_FOLDER_BROWSE,

	/** Output */
	SETTINGS_FOLDER_OUPUT_BORDER,

	/** Output: */
	SETTINGS_FOLDER_OUTPUT_FOLDER,

	/** Output hot key */
	SETTINGS_FOLDER_OUTPUT_FOLDER_HK,

	/** Input */
	SETTINGS_FOLDER_INPUT_BORDER,

	/** Input: */
	SETTINGS_FOLDER_INPUT_FOLDER,

	/** Input hot key */
	SETTINGS_FOLDER_INPUT_FOLDER_HK,

	/** Ack: */
	SETTINGS_FOLDER_ACKNOWLEDGEMENT_FOLDER,

	/** Ack hot key */
	SETTINGS_FOLDER_ACKNOWLEDGEMENT_FOLDER_HK,

	/** Processed: */
	SETTINGS_FOLDER_PROCESSED,

	/** Processed hot key */
	SETTINGS_FOLDER_PROCESSED_HK,

	/** Folder doesn't exist */
	SETTINGS_FOLDER_FOLDER_DOESNT_EXIST,

	/** Server */
	SETTINGS_SERVER_TAB,

	/** Enter connection data */
	SETTINGS_SERVER_DATA,

	/** Enter URL: */
	SETTINGS_SERVER_URL,

	/** URL hot key. */
	SETTINGS_SERVER_URL_HK,

	/** Web service access URL is empty. */
	SETTINGS_SERVER_NO_URL,

	/** Web service access URL must start with https:// */
	SETTINGS_SERVER_NO_HTTPS,

	/** The environment file is invalid. */
	SETTINGS_SERVER_INVALID_ENV_FILE,

	/** Invalid URL. */
	SETTINGS_SERVER_INVALID_URL,

	/** Cannot read environment file. */
	SETTINGS_SERVER_CANNOT_READ_ENV_FILE,

	/** In the configuration file the attribute is mandatory. */
	SETTINGS_SERVER_ATTRIBUTE_IS_MANDATORY,

	/** URL must start with "https://" */
	SETTINGS_SERVER_INVALID_URL_NO_HTTPS,

	/** Other (fill URL value) */
	SETTINGS_SERVER_ENVIRONMENT_OTHER;

	/**
	 * Gets message text.
	 *
	 * @return Message text.
	 */
	public String getMessage(final Object... parameters) {
		return Messages.getString(name(), parameters);
	}

	/**
	 * Return the first chart of the message.
	 *
	 * @return First chart of the message.
	 */
	public char getChar() {
		return Messages.getString(name()).charAt(0);
	}
}
