/*
 * Copyright 2024 Redeia.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTIBIILTY or FITNESS FOR A PARTICULAR PURPOSE. See GNU Lesser General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see
 * http://www.gnu.org/licenses/.
 *
 * Any redistribution and/or modification of this program has to make
 * reference to Redeia as the copyright owner of the program.
 */

package es.ree.eemws.kit.cmd;

import es.ree.eemws.kit.common.Messages;

/**
 * Common message catalog for command line.
 *
 * @author Redeia.
 * @version 2.1 01/01/2024
 */
public enum MessageCatalog {

	/** The application is unable to write the specified file "{0}". Check folder existence and right permissions. */
	UNABLE_TO_WRITE,

	/** The application is unable to read the specified file "{0}". Check file existence and right permissions. */
	UNABLE_TO_READ,

	/** Execution time: {0} */
	EXECUTION_TIME,

	/** Invalid configuration. {0} */
	INVALID_CONFIGURATION,

	/** The given URL "{0}" is not valid. */
	INVALID_URL,

	/** The given parameters are unknown: {0} */
	UNKNOWN_PARAMETERS,

	/** Parameter {0} was repeated. */
	PARAMETER_REPEATED,

	/** Parameter "startTime" */
	PARAMETER_START_TIME,

	/** Parameter "endTime" */
	PARAMETER_END_TIME,

	/** Parameter "code" */
	PARAMETER_CODE,

	/** Parameter "msgId" */
	PARAMETER_MSG_ID,

	/** Parameter "url" */
	PARAMETER_URL,

	/** Parameter "out" */
	PARAMETER_OUT_FILE;

	/**
	 * Gets message text.
	 * @return Message text.
	 */
	public String getMessage(final Object... parameters) {
		return Messages.getString(name(), parameters);
	}

	/**
	 * Return the first chart of the message.
	 * @return First chart of the message.
	 */
	public char getChar() {
		return Messages.getString(name()).charAt(0);
	}



}