/*
 * Copyright 2024 Redeia.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTIBIILTY or FITNESS FOR A PARTICULAR PURPOSE. See GNU Lesser General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see
 * http://www.gnu.org/licenses/.
 *
 * Any redistribution and/or modification of this program has to make
 * reference to Redeia as the copyright owner of the program.
 */

package es.ree.eemws.kit.gui.applications.editor;

import es.ree.eemws.kit.common.Messages;

/**
 * Message codes for the Kit's editor.
 *
 * @author Redeia.
 * @version 2.1 01/01/2024
 */
public enum MessageCatalog {

	/** Cut */
	EDITOR_CUT,

	/** Editor hot key */
	EDITOR_CUT_HK,

	/** Copy */
	EDITOR_COPY,

	/** Copy hot key */
	EDITOR_COPY_HK,

	/** Paste */
	EDITOR_PASTE,

	/** Paste hot key */
	EDITOR_PASTE_HK,

	/** Find */
	EDITOR_FIND,

	/** Find hot key. */
	EDITOR_FIND_HK,

	/** Find text. */
	EDITOR_FIND_NEXT,

	/** Find next hot key. */
	EDITOR_FIND_NEXT_HK,

	/** Replace. */
	EDITOR_REPLACE,

	/** Replace hot key. */
	EDITOR_REPLACE_HK,

	/** Go to line. */
	EDITOR_GO_TO_LINE,

	/** Go to line hot key. */
	EDITOR_GO_TO_LINE_HK,

	/** Undo. */
	EDITOR_UNDO,

	/** Undo hot key. */
	EDITOR_UNDO_HK,

	/** Redo. */
	EDITOR_REDO,

	/** Redo hot key. */
	EDITOR_REDO_HK,

	/** Select all. */
	EDITOR_SELECT_ALL,

	/** Select all hot key. */
	EDITOR_SELECT_ALL_HK,

	/** Select line. */
	EDITOR_SELECT_LINE,

	/** Select line hot key. */
	EDITOR_SELECT_LINE_HK,

	/** Edit menu option. */
	EDITOR_EDIT_MENU_ENTRY,

	/** Edit menu option hot key. */
	EDITOR_EDIT_MENU_ENTRY_HK,

	/** Search term {0} not found in document from character {1} */
	EDITOR_SEARCH_NOT_FOUND_DETAIL,

	/** Not found. */
	EDITOR_SEARCH_NOT_FOUND,

	/** Both terms are the same, there is nothing to replace! */
	EDITOR_REPLACE_THE_SAME,

	/** Nothing to replace. */
	EDITOR_REPLACE_THE_SAME_NOTHING_TO_REPLACE,

	/** {0} occurrence(s) replaced. */
	EDITOR_REPLACE_NUM_REPLACEMENTS,

	/** Document is empty. */
	EDITOR_DOCUMENT_EMPTY,

	/** Enter line number [1-{0}] */
	EDITOR_GO_TO_LINE_NUMBER,

	/** Find: */
	EDITOR_FIND_LBL,

	/** Find hot key. */
	EDITOR_FIND_LBL_HK,

	/** Replace with: */
	EDITOR_REPLACE_LBL,

	/** Replace with hot key. */
	EDITOR_REPLACE_LBL_HK,

	/** Replace all: */
	EDITOR_REPLACE_ALL_LBL,

	/** Replace all hot key */
	EDITOR_REPLACE_ALL_LBL_HK,

	/** Case sensitive */
	EDITOR_REPLACE_CASE_SENSITIVE,

	/** Case sensitive hot key. */
	EDITOR_REPLACE_CASE_SENSITIVE_HK,

	/** Search and replace */
	EDITOR_SEARCH_AND_REPLACE,

	/** Searh and replace hot key */
	EDITOR_SEARCH_AND_REPLACE_HK,

	/** Cancel */
	EDITOR_CANCEL_BUTTON,

	/** Cancel hot key. */
	EDITOR_CANCEL_BUTTON_HK,

	/** New file */
	EDITOR_NEW_FILE_TITLE,

	/** File. */
	EDITOR_MENU_ITEM_FILE,

	/** File hot key. */
	EDITOR_MENU_ITEM_FILE_HK,

	/** New. */
	EDITOR_MENU_ITEM_NEW,

	/** New hot key. */
	EDITOR_MENU_ITEM_NEW_HK,

	/** Open. */
	EDITOR_MENU_ITEM_OPEN,

	/** Open hot key. */
	EDITOR_MENU_ITEM_OPEN_HK,

	/** Save. */
	EDITOR_MENU_ITEM_SAVE,

	/** Save hot key. */
	EDITOR_MENU_ITEM_SAVE_HK,

	/** Save as. */
	EDITOR_MENU_ITEM_SAVE_AS,

	/** Save as hot key. */
	EDITOR_MENU_ITEM_SAVE_AS_HK,

	/** Exit. */
	EDITOR_MENU_ITEM_EXIT,

	/** Exit hot key. */
	EDITOR_MENU_ITEM_EXIT_HK,

	/** Loading file: {0} */
	EDITOR_OPENING_FILE,

	/** Cannot open {0}.\n Make sure file name is correct and you have read permission.\n Error message: {1} */
	EDITOR_CANNOT_OPEN_FILE,

	/** There is no document to save!. */
	EDITOR_NOTHING_TO_SAVE,

	/** File {0} already exists. Do you want to overwrite it? */
	EDITOR_SAVE_FILE_ALREADY_EXISTS,

	/** File {0} was overwritten!. */
	EDITOR_SAVE_FILE_OVERWRITTEN,

	/** File {0} has been saved. */
	EDITOR_SAVE_FILE_SAVED,

	/** Unable to save file {0}.\n Make sure the name entered is correct and you have write permission in the folder. */
	EDITOR_UNABLE_TO_SAVE,

	/** File {0} has been modified.\n Do you want to continue and lose changes? */
	EDITOR_LOSE_CHANGES,

	/** Do you want to exit the editor? */
	EDITOR_EXIT_APPLICATION,

	/** You can only drag files here no folders! */
	EDITOR_CANNOT_LOAD_FOLDER,

	/** Send */
	EDITOR_MENU_ITEM_SEND,

	/** Send hot key. */
	EDITOR_MENU_ITEM_SEND_HK,

	/** Service */
	EDITOR_MENU_ITEM_SERVICE,

	/** Service hot key. */
	EDITOR_MENU_ITEM_SERVICE_HK,

	/** Sending... */
	EDITOR_SENDING,

	/** Acknowledgement message received. */
	EDITOR_ACK_RECEIVED,

	/** Successfully sent in {0} second(s). */
	EDITOR_ACK_OK,

	/** Server rejected the message. See log for details. */
	EDITOR_ACK_NOOK,

	/** Server returns a no IEC-61968100 message. See log for details. */
	EDITOR_NO_IEC_MESSAGE,

	/** Unable to send the document!. See log for details. */
	EDITOR_UNABLE_TO_SEND,

	/** There is no document to send! */
	EDITOR_SEND_DOCUMENT_IS_EMPTY,

	/** Format */
	EDITOR_MENU_ITEM_XML_FORMAT,

	/** Format hot key. */
	EDITOR_MENU_ITEM_XML_FORMAT_HK,

	/** Unable to undo changes. */
	EDITOR_UNABLE_TO_UNDO,

	/** Unable to redo changes. */
	EDITOR_UNABLE_TO_REDO;

	/**
	 * Gets message text.
	 * @return Message text.
	 */
	public String getMessage(final Object... parameters) {
		return Messages.getString(name(), parameters);
	}

	/**
	 * Return the first chart of the message.
	 * @return First chart of the message.
	 */
	public char getChar() {
		return Messages.getString(name()).charAt(0);
	}
}


