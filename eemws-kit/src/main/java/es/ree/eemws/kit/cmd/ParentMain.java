/*
 * Copyright 2024 Redeia.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTIBIILTY or FITNESS FOR A PARTICULAR PURPOSE. See GNU Lesser General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see
 * http://www.gnu.org/licenses/.
 *
 * Any redistribution and/or modification of this program has to make
 * reference to Redeia as the copyright owner of the program.
 */
package es.ree.eemws.kit.cmd;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import es.ree.eemws.core.utils.config.ConfigException;
import es.ree.eemws.kit.config.Configuration;

/**
 * Parent class of the line command actions.
 *
 * @author Redeia.
 * @version 2.1 01/01/2024
 */
public abstract class ParentMain {

	/** Configuration options. */
	private static Configuration config = null;

	/** Date format. */
	protected static final String DATE_FORMAT_PATTERN = "dd-MM-yyyy"; //$NON-NLS-1$

	/** Performance format without minutes. */
	private static final String FORMAT_WITHOUT_MINUTES = "%02d\".%03d";

	/** Performance format with minutes. */
	private static final String FORMAT_WITH_MINUTES = "%d'%02d\".%03d";

	/**
	 * Returns the first parameter name duplicated in the given list.
	 *
	 * @param args            Argument list.
	 * @param parametersNames Expected parameter names (others will be ignored)
	 * @return First element duplicated in the given list. <code>null</code> if
	 *         there is no duplicate.
	 */
	protected static String findDuplicates(final List<String> args, final String... parametersNames) {

		String retValue = null;
		Set<String> uniq = new HashSet<>();
		List<String> copy = new ArrayList<>(args);
		copy.retainAll(Arrays.asList(parametersNames));

		var size = copy.size();
		for (var k = 0; k < size && retValue == null; k++) {
			if (!uniq.add(copy.get(k))) {
				retValue = copy.get(k);
			}
		}

		return retValue;
	}

	/**
	 * Returns the value of a command line parameter given the list of the
	 * parameters and the prefix. If list of parameters contain the prefix, the
	 * method will return the value n+1 where n is the position of the prefix in the
	 * list. Then both key and value are removed form the list. The class that
	 * invokes this method can check that there is no remain values in the list, if
	 * so, that means that the users puts some "unknown" value, then execution must
	 * be finished.
	 *
	 * @param args   List of the command line arguments.
	 * @param prefix Prefix of the argument.
	 * @return Value of the argument, <code>null</code> if the parameter was not
	 *         specified.
	 */
	protected static String readParameter(final List<String> args, final String prefix) {

		String retValue = null;

		var len = args.size() - 1;

		for (var cont = 0; retValue == null && cont < len; cont++) {
			if (args.get(cont).equals(prefix)) {
				retValue = args.get(cont + 1);
				args.remove(cont); // Remove key.
				args.remove(cont); // Then value.
			}
		}

		return retValue;
	}

	/**
	 * This method set the configuration client.
	 *
	 * @param endPoint URL end point if <code>null</code> will overrides the one set
	 *                 up in the configuration.
	 * @return URL end point.
	 * @throws ConfigException Exception with the error.
	 */
	protected static String setConfig(final String endPoint) throws ConfigException {

		config = new Configuration();
		config.readConfiguration();

		var endPnt = endPoint;
		if (endPnt == null) {

			endPnt = config.getUrlEndPoint().toString();
		}

		return endPnt;
	}

	/**
	 * This method gets the configuration options.
	 *
	 * @return Configuration options.
	 */
	protected static Configuration getConfig() {

		return config;
	}

	/**
	 * This method get the execution time.
	 *
	 * @param init Initial time.
	 * @param end  End time.
	 * @return String with the format [yy'zz".mmm].
	 */
	protected static String getPerformance(final long init, final long end) {

		String retValue;

		var dif = end - init;
		var seg = (int) dif / 1000;
		var minutes = seg / 60;
		seg = seg % 60;
		long milliseconds = (int) dif % 1000;

		if (minutes > 0) {
			retValue = String.format(FORMAT_WITH_MINUTES, minutes, seg, milliseconds);
		} else {
			retValue = String.format(FORMAT_WITHOUT_MINUTES, minutes, seg, milliseconds);
		}

		return retValue;
	}
}
