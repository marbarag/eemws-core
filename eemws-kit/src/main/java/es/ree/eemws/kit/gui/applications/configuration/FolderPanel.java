/*
 * Copyright 2024 Redeia.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTIBIILTY or FITNESS FOR A PARTICULAR PURPOSE. See GNU Lesser General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see
 * http://www.gnu.org/licenses/.
 *
 * Any redistribution and/or modification of this program has to make
 * reference to Redeia as the copyright owner of the program.
 */

package es.ree.eemws.kit.gui.applications.configuration;

import java.awt.Color;
import java.awt.Rectangle;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import es.ree.eemws.core.utils.config.ConfigException;
import es.ree.eemws.kit.folders.BasicConfiguration;

/**
 * Magic Folder configuration tab.
 *
 * @author Redeia.
 * @version 2.1 01/01/2024
 */
public final class FolderPanel extends JPanel {

	/** Class ID. */
	private static final long serialVersionUID = -1770175870546621003L;

	/** Text box where absolute path to input folder is entered. */
	private JTextField txtInputFolderPath = null;

	/** Text box where absolute path to response folder is entered. */
	private JTextField txtResponseFolderPath = null;

	/** Text box where absolute path to processing folder is entered. */
	private JTextField txtProcessedFolderPath = null;

	/** Text box where absolute path to output folder is entered. */
	private JTextField txtOutputFolderPath = null;

	/** Text box where absolute path to backup folder is entered. */
	private JTextField txtBackupFolderPath = null;

	/**
	 * Obtains panel containing folder settings.
	 */
	public FolderPanel() {
		setLayout(null);
		setOpaque(true);
		add(getInputPanel(), null);
		add(getOutputPanel(), null);
		add(getBackupPanel(), null);
	}

	/**
	 * Obtain sub-panel containing settings for backup folder.
	 *
	 * @return Sub-panel containing settings for backup folder.
	 */
	private JPanel getBackupPanel() {

		txtBackupFolderPath = new JTextField(""); //$NON-NLS-1$
		txtBackupFolderPath.setBounds(new Rectangle(90, 20, 320, 20));

		var backupLbl = new JLabel(MessageCatalog.SETTINGS_FOLDER_BACKUP_FOLDER.getMessage());
		backupLbl.setDisplayedMnemonic(MessageCatalog.SETTINGS_FOLDER_BACKUP_FOLDER_HK.getChar());
		backupLbl.setLabelFor(txtBackupFolderPath);
		backupLbl.setHorizontalAlignment(SwingConstants.RIGHT);
		backupLbl.setBounds(new Rectangle(19, 20, 70, 20));

		var backupBtn = new JButton("..."); //$NON-NLS-1$
		backupBtn.setBounds(new Rectangle(420, 20, 30, 20));
		backupBtn.setToolTipText(MessageCatalog.SETTINGS_FOLDER_BROWSE.getMessage());
		backupBtn.addActionListener(e -> examine(txtBackupFolderPath));

		var backupPanel = new JPanel();
		backupPanel.setLayout(null);
		backupPanel.setBorder(
		        new TitledBorder(new EtchedBorder(EtchedBorder.RAISED, Color.white, new Color(142, 142, 142)),
		                " " + MessageCatalog.SETTINGS_FOLDER_BACKUP_BORDER.getMessage() + " ")); //$NON-NLS-1$//$NON-NLS-2$
		backupPanel.setDebugGraphicsOptions(0);
		backupPanel.setBounds(new Rectangle(19, 175, 469, 55));
		backupPanel.add(txtBackupFolderPath, null);
		backupPanel.add(backupLbl, null);
		backupPanel.add(backupBtn, null);

		return backupPanel;
	}

	/**
	 * Obtain sub-panel containing settings for output folder.
	 *
	 * @return Sub-panel containing settings for output folder.
	 */
	private JPanel getOutputPanel() {

		txtOutputFolderPath = new JTextField(""); //$NON-NLS-1$
		txtOutputFolderPath.setBounds(new Rectangle(90, 20, 320, 20));

		var lblOutput = new JLabel(MessageCatalog.SETTINGS_FOLDER_OUTPUT_FOLDER.getMessage());
		lblOutput.setDisplayedMnemonic(MessageCatalog.SETTINGS_FOLDER_OUTPUT_FOLDER_HK.getChar());
		lblOutput.setLabelFor(txtOutputFolderPath);
		lblOutput.setHorizontalAlignment(SwingConstants.RIGHT);
		lblOutput.setBounds(new Rectangle(19, 20, 70, 20));

		var btnOutput = new JButton("..."); //$NON-NLS-1$
		btnOutput.setBounds(new Rectangle(420, 20, 30, 20));
		btnOutput.setToolTipText(MessageCatalog.SETTINGS_FOLDER_BROWSE.getMessage());
		btnOutput.addActionListener(e -> examine(txtOutputFolderPath));

		var pnlOutput = new JPanel();
		pnlOutput.setLayout(null);
		pnlOutput.setBorder(
		        new TitledBorder(new EtchedBorder(EtchedBorder.RAISED, Color.white, new Color(142, 142, 142)),
		                " " + MessageCatalog.SETTINGS_FOLDER_OUPUT_BORDER.getMessage() + " ")); //$NON-NLS-1$//$NON-NLS-2$
		pnlOutput.setDebugGraphicsOptions(0);
		pnlOutput.setBounds(new Rectangle(19, 116, 469, 55));
		pnlOutput.add(txtOutputFolderPath, null);
		pnlOutput.add(lblOutput, null);
		pnlOutput.add(btnOutput, null);

		return pnlOutput;
	}

	/**
	 * Obtain sub-panel containing settings for output folder.
	 *
	 * @return Sub-panel containing settings for output folder.
	 */
	public JPanel getInputPanel() {

		txtInputFolderPath = new JTextField(""); //$NON-NLS-1$
		txtInputFolderPath.setBounds(new Rectangle(90, 20, 320, 20));

		var lblInput = new JLabel(MessageCatalog.SETTINGS_FOLDER_INPUT_FOLDER.getMessage());
		lblInput.setHorizontalAlignment(SwingConstants.RIGHT);
		lblInput.setDisplayedMnemonic(MessageCatalog.SETTINGS_FOLDER_INPUT_FOLDER_HK.getChar());
		lblInput.setLabelFor(txtInputFolderPath);
		lblInput.setBounds(new Rectangle(19, 20, 70, 20));

		var btnInput = new JButton("..."); //$NON-NLS-1$
		btnInput.setBounds(new Rectangle(420, 20, 30, 20));
		btnInput.setToolTipText(MessageCatalog.SETTINGS_FOLDER_BROWSE.getMessage());
		btnInput.addActionListener(e -> examine(txtInputFolderPath));

		txtResponseFolderPath = new JTextField(""); //$NON-NLS-1$
		txtResponseFolderPath.setBounds(new Rectangle(90, 45, 320, 20));

		var lblResponse = new JLabel(MessageCatalog.SETTINGS_FOLDER_ACKNOWLEDGEMENT_FOLDER.getMessage());
		lblResponse.setDisplayedMnemonic(MessageCatalog.SETTINGS_FOLDER_ACKNOWLEDGEMENT_FOLDER_HK.getChar());
		lblResponse.setHorizontalAlignment(SwingConstants.RIGHT);
		lblResponse.setLabelFor(txtResponseFolderPath);
		lblResponse.setBounds(new Rectangle(19, 45, 70, 20));

		var btnResponse = new JButton("..."); //$NON-NLS-1$
		btnResponse.setBounds(new Rectangle(420, 45, 30, 20));
		btnResponse.setToolTipText(MessageCatalog.SETTINGS_FOLDER_BROWSE.getMessage());
		btnResponse.addActionListener(e -> examine(txtResponseFolderPath));

		txtProcessedFolderPath = new JTextField(""); //$NON-NLS-1$
		txtProcessedFolderPath.setBounds(new Rectangle(90, 70, 320, 20));

		var lblProcessed = new JLabel(MessageCatalog.SETTINGS_FOLDER_PROCESSED.getMessage());
		lblProcessed.setHorizontalAlignment(SwingConstants.RIGHT);
		lblProcessed.setDisplayedMnemonic(MessageCatalog.SETTINGS_FOLDER_PROCESSED_HK.getChar());
		lblProcessed.setLabelFor(txtProcessedFolderPath);
		lblProcessed.setBounds(new Rectangle(19, 70, 70, 20));

		var btnProcesed = new JButton("..."); //$NON-NLS-1$
		btnProcesed.setBounds(new Rectangle(420, 70, 30, 20));
		btnProcesed.setToolTipText(MessageCatalog.SETTINGS_FOLDER_BROWSE.getMessage());
		btnProcesed.addActionListener(e -> examine(txtProcessedFolderPath));

		var pnlInput = new JPanel();
		pnlInput.setLayout(null);
		pnlInput.setBorder(
		        new TitledBorder(new EtchedBorder(EtchedBorder.RAISED, Color.white, new Color(142, 142, 142)),
		                " " + MessageCatalog.SETTINGS_FOLDER_INPUT_BORDER.getMessage() + " ")); //$NON-NLS-1$ //$NON-NLS-2$

		pnlInput.setBounds(new Rectangle(19, 12, 469, 100));
		pnlInput.add(txtInputFolderPath, null);
		pnlInput.add(lblInput, null);
		pnlInput.add(btnInput, null);
		pnlInput.add(txtResponseFolderPath, null);
		pnlInput.add(lblResponse, null);
		pnlInput.add(btnResponse, null);
		pnlInput.add(txtProcessedFolderPath, null);
		pnlInput.add(lblProcessed, null);
		pnlInput.add(btnProcesed, null);

		return pnlInput;
	}

	/**
	 * Open file chooser to select path to certificate file.
	 *
	 * @param textField Text box to which file path will be written.
	 */
	private void examine(final JTextField textField) {

		var fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		fileChooser.setSelectedFile(new File(textField.getText()));

		if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {

			var file = fileChooser.getSelectedFile();
			if (file.isDirectory()) {

				textField.setText(changeFileNameSeparator(file.getAbsolutePath()));

			} else {

				JOptionPane.showMessageDialog(this,
				        es.ree.eemws.kit.gui.common.MessageCatalog.MSG_ERROR_TITLE.getMessage(),
				        MessageCatalog.SETTINGS_FOLDER_FOLDER_DOESNT_EXIST.getMessage(), JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	/**
	 * Changes Windows file path (a\b\c) to Unix file path (a/b/c).
	 *
	 * @param filename Absolute file name to convert.
	 * @return New file name changing '\' character into '/'
	 */
	private String changeFileNameSeparator(final String filename) {
		String retValue;

		if (filename == null) {
			retValue = null;
		} else {
			retValue = filename.replaceAll("\\\\", "/"); //$NON-NLS-1$ //$NON-NLS-2$
		}

		return retValue;
	}

	/**
	 * Loads path names of folders into their respective form fields.
	 *
	 * @throws ConfigException If settings cannot be read.
	 */
	public void loadValues() throws ConfigException {

		var cf = new BasicConfiguration();
		cf.readConfiguration();
		txtInputFolderPath.setText(changeFileNameSeparator(cf.getInputFolder()));
		txtResponseFolderPath.setText(changeFileNameSeparator(cf.getResponseFolder()));
		txtProcessedFolderPath.setText(changeFileNameSeparator(cf.getProcessedFolder()));
		txtOutputFolderPath.setText(changeFileNameSeparator(cf.getOutputFolder()));
		txtBackupFolderPath.setText(changeFileNameSeparator(cf.getBackupFolder()));
	}

	/**
	 * Sets folder setting values.
	 */
	public void setValues() {

		var cf = new BasicConfiguration();

		try {
			cf.setInputFolder(changeFileNameSeparator(txtInputFolderPath.getText()));
			cf.setResponseFolder(changeFileNameSeparator(txtResponseFolderPath.getText()));
			cf.setProcessedFolder(changeFileNameSeparator(txtProcessedFolderPath.getText()));
			cf.setBackupFolder(changeFileNameSeparator(txtBackupFolderPath.getText()));
			cf.setOutputFolder(changeFileNameSeparator(txtOutputFolderPath.getText()));
			cf.writeConfiguration();

		} catch (ConfigException ex) {

			/*
			 * Settings should be already validated at this point, thus this exception
			 * should be unreachable.
			 */
			JOptionPane.showMessageDialog(this, ex.toString(),
			        es.ree.eemws.kit.gui.common.MessageCatalog.MSG_ERROR_TITLE.getMessage(), JOptionPane.ERROR_MESSAGE);
		}

	}

	/**
	 * Validates folder panel.
	 *
	 * @throws ConfigException If entered folders are incorrect.
	 */
	public void validateConfig() throws ConfigException {

		var cf = new BasicConfiguration();

		var inputFolderPath = changeFileNameSeparator(txtInputFolderPath.getText().trim());
		if (inputFolderPath != null && inputFolderPath.length() == 0) {
			inputFolderPath = null;
		}
		cf.setInputFolder(inputFolderPath);

		var responseFolderPath = changeFileNameSeparator(txtResponseFolderPath.getText().trim());
		if (responseFolderPath != null && responseFolderPath.length() == 0) {
			responseFolderPath = null;
		}
		cf.setResponseFolder(responseFolderPath);

		var processingFolderPath = changeFileNameSeparator(txtProcessedFolderPath.getText().trim());
		if (processingFolderPath != null && processingFolderPath.length() == 0) {
			processingFolderPath = null;
		}
		cf.setProcessedFolder(processingFolderPath);

		var outputFolderPath = changeFileNameSeparator(txtOutputFolderPath.getText().trim());
		if (outputFolderPath != null && outputFolderPath.length() == 0) {
			outputFolderPath = null;
		}
		cf.setOutputFolder(outputFolderPath);

		var backupFolderPath = changeFileNameSeparator(txtBackupFolderPath.getText().trim());
		if (backupFolderPath != null && backupFolderPath.length() == 0) {
			backupFolderPath = null;
		}
		cf.setBackupFolder(backupFolderPath);

		try {
			cf.validateConfiguration();
		} catch (ConfigException ex) {
			throw new ConfigException(getPanelName() + " " + MessageCatalog.SETTINGS_PANEL_SAYS.getMessage() + " " //$NON-NLS-1$
			        + ex.getMessage());
		}
	}

	/**
	 * Returns panel name.
	 *
	 * @return Panel name.
	 */
	public String getPanelName() {

		return MessageCatalog.SETTINGS_FOLDER_TAB.getMessage();
	}
}
