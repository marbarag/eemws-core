/*
 * Copyright 2024 Redeia.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTIBIILTY or FITNESS FOR A PARTICULAR PURPOSE. See GNU Lesser General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see
 * http://www.gnu.org/licenses/.
 *
 * Any redistribution and/or modification of this program has to make
 * reference to Redeia as the copyright owner of the program.
 */

package es.ree.eemws.kit.gui.applications.browser;

/**
 * Enumeration with the message list table view columns.
 *
 * @author Redeia.
 * @version 2.1 01/01/2024
 *
 */
public enum ColumnsId {

	/** Column Code. */
	CODE(MessageCatalog.BROWSER_COLUMN_CODE.getMessage()),

	/** Column Id. */
	ID(MessageCatalog.BROWSER_COLUMN_ID.getMessage()),

	/** Column Version. */
	VERSION(MessageCatalog.BROWSER_COLUMN_VERSION.getMessage()),

	/** Column Status. */
	STATUS(MessageCatalog.BROWSER_COLUMN_STATUS.getMessage()),

	/** Column Start Time. */
	APPLICATION_ST_TIME(MessageCatalog.BROWSER_COLUMN_APPLICATION_ST_TIME.getMessage()),

	/** Column End Time. */
	APPLICATION_END_TIME(MessageCatalog.BROWSER_COLUMN_APPLICATION_END_TIME.getMessage()),

	/** Column Server Timestamp. */
	SERVER_TIMESTAMP(MessageCatalog.BROWSER_COLUMN_SERVER_TIMESTAMP.getMessage()),

	/** Column Type. */
	TYPE(MessageCatalog.BROWSER_COLUMN_MSG_TYPE.getMessage()),

	/** Column Owner. */
	OWNER(MessageCatalog.BROWSER_COLUMN_OWNER.getMessage());

	/** Column text (visible text). */
	private final String columnText;

	/**
	 * Constructor. Sets the visible text column.
	 *
	 * @param text Visible text column.
	 */
	ColumnsId(final String text) {
		columnText = text;
	}

	/**
	 * Return the visible text column.
	 *
	 * @return Visible text column.
	 */
	public String getText() {
		return columnText;
	}
}
