/*
 * Copyright 2024 Redeia.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTIBIILTY or FITNESS FOR A PARTICULAR PURPOSE. See GNU Lesser General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see
 * http://www.gnu.org/licenses/.
 *
 * Any redistribution and/or modification of this program has to make
 * reference to Redeia as the copyright owner of the program.
 */

package es.ree.eemws.kit.cmd.trustserver;

import es.ree.eemws.kit.common.Messages;

/**
 * Common message catalog for command line.
 *
 * @author Redeia.
 * @version 2.1 01/01/2024
 */
public enum TrustServerMessageCatalog {

	/** Server did not return certificate chain. Unable to trust server. (Tip: Try to disable SNI Extension in commEnv script) */
	TRUSTSERVER_NO_CERT_CHAIN,

	/** New server certificate was added, you must re-run the command to retrieve signature certificate from the new server. */
	TRUSTSERVER_RERUN_COMMAND,

	/** Getting server certificates... */
	TRUSTSERVER_GETTING_SERVER_CERTICATES,

	/** Getting signature certificates... */
	TRUSTSERVER_GETTING_SIGNATURE_CERTICATES,

	/** Cannot retrieve signature certificate: Unable to connect with the server. */
	TRUSTSERVER_UNABLE_TO_CONNECT_WITH_SERVER,

	/** Retrieving message {0}... */
	TRUSTSERVER_MSG_GET,

	/** {0} messages available in the server. */
	TRUSTSERVER_MSG_LIST,

	/** Getting messages list (this may take awhile)... */
	TRUSTSERVER_GETTING_MSG_LIST,

	/** signature */
	TRUSTSERVER_SIGNATURE,

	/** There are no messages in the server. The server's certificate for signature couldn't be retrieved. */
	TRUSTSERVER_NO_MESSAGES_TO_LIST,

	/** Unable to connect with the given server {0}. Check system connectivity settings, check url. Check DNS settings. */
	TRUSTSERVER_UNKNOW_HOST,

	/** Unable to connect with the given server {0}. Check system connectivity settings, check url. */
	TRUSTSERVER_CANNOT_CONNECT,

	/** Unable to use the keystore!. Check if there is other process using this configuration. Check if you have write rights. */
	TRUSTSERVER_BAD_KEYSTORE,

	/** Skipping...{0} issued by {1}(Certificate not yet valid) */
	TRUSTSERVER_SKIPPING_NOT_YET_VALID,

	/** Skipping...{0} issued by {1} (Certificate expired) */
	TRUSTSERVER_SKIPPING_EXPIRED,

	/** Skipping...{0} (already added) */
	TRUSTSERVER_SKIPPING_ALREADY_ADDED,

	/** Skipping...{0} issued by {1} (already trusted) */
	TRUSTSERVER_SKIPPING_CERTIFICATE,

	/** Adding...{0} issued by {1} */
	TRUSTSERVER_ADDING_CERTIFICATE,

	/** Opening connection to: {0} */
	TRUSTSERVER_OPENING_CONNECTION,

	/** Local trust key store has {0} certificates. */
	TRUSTSERVER_TRUST_SIZE,

	/** Unable to save keystore. Check if there is other process using this configuration. */
	TRUSTSERVER_UNABLE_TO_SAVE,

	/** Unable to load keystore. */
	TRUSTSERVER_UNABLE_TO_LOAD,

	/** No trust store is configured. Check system configuration. */
	TRUSTSERVER_NO_TRUST_STORE,

	/** Only https protocol is allowed. */
	TRUSTSERVER_ONLY_HTTPS,

	/** Trust Server usage. */
	TRUSTSERVER_USAGE;

	/**
	 * Gets message text.
	 * @return Message text.
	 */
	public String getMessage(final Object... parameters) {
		return Messages.getString(name(), parameters);
	}

	/**
	 * Return the first chart of the message.
	 * @return First chart of the message.
	 */
	public char getChar() {
		return Messages.getString(name()).charAt(0);
	}



}