/*
 * Copyright 2024 Redeia.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTIBIILTY or FITNESS FOR A PARTICULAR PURPOSE. See GNU Lesser General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see
 * http://www.gnu.org/licenses/.
 *
 * Any redistribution and/or modification of this program has to make
 * reference to Redeia as the copyright owner of the program.
 */
package es.ree.eemws.kit.gui.common;

import java.io.Serializable;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

/**
 * Log Window management.
 *
 * @author Redeia.
 * @version 2.1 01/01/2024
 */
public final class LogHandle implements LoggerListener, Serializable {

	/** Serial version UID. */
	private static final long serialVersionUID = 3380927491446921383L;

	/** Log menu item, is modified depending on status Log status. */
	private JCheckBoxMenuItem miShowLog = new JCheckBoxMenuItem();

	/** Log window. */
	private Logger log;

	/** Log menu. */
	private JMenu mnLogMenu = new JMenu();

	/**
	 * Creates new instance of Log window.
	 */
	public LogHandle() {
		log = new Logger(this);
		log.setVisible(false);
	}

	/**
	 * Retrieves "Log" menu.
	 * @return "Log" menu.
	 */
	public JMenu getMenu() {

		var miDeleteLog = new JMenuItem();

		miDeleteLog.setText(MessageCatalog.LOG_CLEAR_MENU_ITEM.getMessage());
		miDeleteLog.setMnemonic(MessageCatalog.LOG_CLEAR_MENU_ITEM_HK.getChar());
		miDeleteLog.addActionListener(e -> log.deleteLog());

		miShowLog.setText(MessageCatalog.LOG_SHOW_MENU_ITEM.getMessage());
		miShowLog.setMnemonic(MessageCatalog.LOG_SHOW_MENU_ITEM_HK.getChar());
		miShowLog.addActionListener(e -> log.setVisible(miShowLog.isSelected()));

		mnLogMenu.setText(MessageCatalog.LOG_MENU_ITEM.getMessage());
		mnLogMenu.setMnemonic(MessageCatalog.LOG_MENU_ITEM_HK.getChar());
		mnLogMenu.add(miShowLog);
		mnLogMenu.add(miDeleteLog);

		return mnLogMenu;
	}


	/**
	 * Retrieves reference to log Window.
	 * @return Reference to log Window.
	 */
	public Logger getLog() {
		return log;
	}

	/**
	 * Invoked by window when closing.
	 */
	@Override
	public void logWindowIsClosing() {
		miShowLog.setSelected(false);
	}

	/**
	 * Enables / disables graphic values.
	 * @param activeValue <code>true</code> Enable.
	 * <code>false</code> disable.
	 */
	public void enable(final boolean activeValue) {
		var subMenu = mnLogMenu.getMenuComponents();
		for (var cont = 0; cont < subMenu.length; cont++) {
			subMenu[cont].setEnabled(activeValue);
		}
	}
}
