/*
 * Copyright 2024 Redeia.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTIBIILTY or FITNESS FOR A PARTICULAR PURPOSE. See GNU Lesser General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see
 * http://www.gnu.org/licenses/.
 *
 * Any redistribution and/or modification of this program has to make
 * reference to Redeia as the copyright owner of the program.
 */

package es.ree.eemws.kit.gui.applications.configuration;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.UIManager;
import javax.swing.WindowConstants;

import es.ree.eemws.core.utils.config.ConfigException;
import es.ree.eemws.kit.config.Configuration;

/**
 * Configuration app main class.
 *
 * @author Redeia.
 * @version 2.1 01/01/2024
 */
public final class Configurator extends JFrame {

	/** Class ID. */
	private static final long serialVersionUID = -6230590140237153111L;

	/** Certificate settings Panel. */
	private IdentityPanel identityPanel;

	/** Proxy access Settings. */
	private ProxyPanel proxyPanel;

	/** Server and service settings panel . */
	private ServerPanel serverPanel;

	/** Magic folder settings panel. */
	private FolderPanel folderPanel;

	/**
	 * Starts Application.
	 *
	 * @param args -Ignored-
	 */
	public static void main(final String[] args) {
		var config = new Configurator();
		config.setVisible(true);
	}

	/**
	 * Constructor. Invoke creation graphic elements, read current settings and
	 * update elements according to this settings.
	 */
	public Configurator() {

		try {

			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

		} catch (Exception ex) {

			/*
			 * ClassNotFoundException, InstantiationException, IllegalAccessException,
			 * UnsupportedLookAndFeelException This block should be unreachable.
			 */

			JOptionPane.showMessageDialog(this, MessageCatalog.SETTINGS_NO_GUI.getMessage() + ex.getMessage(),
			        es.ree.eemws.kit.gui.common.MessageCatalog.MSG_ERROR_TITLE.getMessage(), JOptionPane.ERROR_MESSAGE);
		}

		var commonConfig = new Configuration();

		try {

			commonConfig.readConfiguration();

		} catch (ConfigException ex) {

			JOptionPane.showMessageDialog(this, MessageCatalog.SETTINGS_NO_CONFIG.getMessage(),
			        es.ree.eemws.kit.gui.common.MessageCatalog.MSG_INFO_TITLE.getMessage(),
			        JOptionPane.INFORMATION_MESSAGE);

			Logger.getLogger(getClass().getName()).log(Level.FINE, "", ex); //$NON-NLS-1$
		}

		identityPanel = new IdentityPanel();
		identityPanel.loadValues(commonConfig);

		proxyPanel = new ProxyPanel();
		proxyPanel.loadValues(commonConfig);

		serverPanel = new ServerPanel();
		serverPanel.loadValues(commonConfig);

		folderPanel = new FolderPanel();
		try {
			folderPanel.loadValues();
		} catch (ConfigException ex) {

			/* Ignore errors on load. */
			Logger.getLogger(getClass().getName()).log(Level.FINE, "", ex); //$NON-NLS-1$
		}

		initGraphicElements();
	}

	/**
	 * Initializes graphic elements. Create and arrange elements on screen.
	 */
	private void initGraphicElements() {

		setIconImage(Toolkit.getDefaultToolkit()
		        .getImage(getClass().getResource(es.ree.eemws.kit.gui.common.Constants.ICON_PATH)));

		/* Adding buttons */
		getContentPane().add(getButtonPanel(), BorderLayout.SOUTH);

		/* Adding central panel (tab panel) */
		getContentPane().add(getMainPanel(), BorderLayout.CENTER);

		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

		setTitle(MessageCatalog.SETTINGS_TITLE.getMessage());

		/* Event triggered when window closes. */
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent e) { // NOSONAR event is not used.
				cancelChanges();
			}
		});

		setResizable(false);
		setSize(520, 360);
		var screen = new Dimension(Toolkit.getDefaultToolkit().getScreenSize());
		setLocation(screen.width / 2 - getSize().width / 2, screen.height / 2 - getSize().height / 2);
	}

	/**
	 * Returns panel containing every settings tab.
	 *
	 * @return Panel Panel containing settings tabs.
	 */
	private JTabbedPane getMainPanel() {

		var main = new JTabbedPane();
		main.setMinimumSize(new Dimension(500, 500));
		main.add(serverPanel, serverPanel.getPanelName());
		main.add(identityPanel, identityPanel.getPanelName());
		main.add(proxyPanel, proxyPanel.getPanelName());
		main.add(folderPanel, folderPanel.getPanelName());

		return main;
	}

	/**
	 * Returns panel contaning 'OK' and 'Cancel' buttons.
	 *
	 * @return Panel containing these common buttons.
	 */
	private JPanel getButtonPanel() {

		var accept = new JButton();
		accept.setMaximumSize(new Dimension(100, 26));
		accept.setMinimumSize(new Dimension(100, 26));
		accept.setPreferredSize(new Dimension(100, 26));
		accept.setToolTipText(MessageCatalog.SETTINGS_OK_BUTTON_TIP.getMessage());
		accept.setIcon(null);
		accept.setMnemonic(MessageCatalog.SETTINGS_OK_BUTTON_HK.getChar());
		accept.setText(MessageCatalog.SETTINGS_OK_BUTTON.getMessage());
		accept.addActionListener(e -> saveChanges());

		var buttonSeparator = new JLabel();
		buttonSeparator.setText("          "); //$NON-NLS-1$
		var cancel = new JButton();
		cancel.setMaximumSize(new Dimension(100, 26));
		cancel.setMinimumSize(new Dimension(100, 26));
		cancel.setPreferredSize(new Dimension(100, 26));
		cancel.setMnemonic(MessageCatalog.SETTINGS_CANCEL_BUTTON_HK.getChar());
		cancel.setText(MessageCatalog.SETTINGS_CANCEL_BUTTON.getMessage());
		cancel.setToolTipText(MessageCatalog.SETTINGS_CANCEL_BUTTON_TIP.getMessage());
		cancel.addActionListener(e -> cancelChanges());

		var buttonPanel = new JPanel();
		buttonPanel.add(accept, null);
		buttonPanel.add(buttonSeparator, null);
		buttonPanel.add(cancel, null);
		return buttonPanel;
	}

	/**
	 * Method triggered when 'Cancel' button is pressed. Exit program without making
	 * changes.
	 */
	private void cancelChanges() {

		var respuesta = JOptionPane.showConfirmDialog(this, MessageCatalog.SETTINGS_CANCEL_TEXT.getMessage(),
		        es.ree.eemws.kit.gui.common.MessageCatalog.MSG_CONFIRM_TITLE.getMessage(), JOptionPane.OK_CANCEL_OPTION,
		        JOptionPane.QUESTION_MESSAGE);

		if (respuesta == JOptionPane.OK_OPTION) {

			System.exit(0); // NOSONAR We want to force application to exit.
		}
	}

	/**
	 * Method triggered when 'OK' button is pressed. Save changes and exit.
	 */
	private void saveChanges() {

		var res = JOptionPane.showConfirmDialog(this, MessageCatalog.SETTINGS_SAVE_CHANGES.getMessage(),
		        es.ree.eemws.kit.gui.common.MessageCatalog.MSG_CONFIRM_TITLE.getMessage(), JOptionPane.OK_CANCEL_OPTION,
		        JOptionPane.QUESTION_MESSAGE);

		if (res == JOptionPane.OK_OPTION) {

			try {
				serverPanel.validateConfig();
				identityPanel.validateConfig();
				proxyPanel.validateConfig();

				if (validateMagicFolder()) {
					saveConfig();
					System.exit(0); // NOSONAR We want to force application to exit.
				}

			} catch (ConfigException ex) {

				JOptionPane.showMessageDialog(this,
				        MessageCatalog.SETTINGS_CONFIG_HAS_ERRORS.getMessage() + ex.getMessage(),
				        es.ree.eemws.kit.gui.common.MessageCatalog.MSG_ERROR_TITLE.getMessage(),
				        JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	/**
	 * Validate MagicFolder settings. If those settings are invalid, the user can
	 * continue and save the configuration as it is. This behaviour is different
	 * from the other tab where the information must be valid to be saved.
	 *
	 * @return <code>true</code> if the configuration is valid or if the user want
	 *         to save anyway. <code>false</code> otherwise.
	 */
	private boolean validateMagicFolder() {

		var wantToSave = true;

		try {
			folderPanel.validateConfig();
		} catch (ConfigException e) {
			var res = JOptionPane.showConfirmDialog(this,
			        MessageCatalog.SETTINGS_CONFIG_MAGIC_FOLDER.getMessage() + e.getMessage(),
			        es.ree.eemws.kit.gui.common.MessageCatalog.MSG_WARNING_TITLE.getMessage(),
			        JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);

			wantToSave = (res == JOptionPane.OK_OPTION);

		}
		return wantToSave;
	}

	/**
	 * Saves settings file.
	 *
	 * @throws ConfigException If settings file cannot be saved.
	 */
	private void saveConfig() throws ConfigException {

		var config = new Configuration();
		identityPanel.setValues(config);
		serverPanel.setValues(config);
		proxyPanel.setValues(config);

		config.writeConfiguration();

		folderPanel.setValues();
	}

}
