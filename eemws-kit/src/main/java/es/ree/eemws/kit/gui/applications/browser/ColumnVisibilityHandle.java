/*
 * Copyright 2024 Redeia.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTIBIILTY or FITNESS FOR A PARTICULAR PURPOSE. See GNU Lesser General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see
 * http://www.gnu.org/licenses/.
 *
 * Any redistribution and/or modification of this program has to make
 * reference to Redeia as the copyright owner of the program.
 */
package es.ree.eemws.kit.gui.applications.browser;

import java.util.ArrayList;
import java.util.prefs.Preferences;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

/**
 * Handles column visibility status.
 *
 * @author Redeia.
 * @version 2.1 01/01/2024
 */
public final class ColumnVisibilityHandle {

	/** Column visibility in "simple" display. */
	private static final boolean[] SIMPLE_VIEW = { true, true, true, true, false, false, false, false, false };

	/** Menu element array containing column visibility status. */
	private JCheckBoxMenuItem[] arColumnsMenuItem;

	/** Reference to table model. */
	private ListTableModel tableModel = null;

	/** Preferences object to save visibility status. */
	private Preferences preferences;

	/**
	 * Constructor. Retrieve visualization preferences.
	 *
	 * @param pTableModel Table model on which show / hide columns.
	 */
	public ColumnVisibilityHandle(final ListTableModel pTableModel) {
		tableModel = pTableModel;
		preferences = Preferences.userNodeForPackage(getClass());
	}

	/**
	 * Add visualization options to the menu passed as parameter.
	 *
	 * @param menuVer Visualization option main menu.
	 */
	public void getMenu(final JMenu menuVer) {

		var mnColumnMenu = new JMenu(MessageCatalog.BROWSER_COLUMN_MENU_ENTRY.getMessage());
		mnColumnMenu.setMnemonic(MessageCatalog.BROWSER_COLUMN_MENU_ENTRY_HK.getChar());
		menuVer.add(mnColumnMenu);

		var simpleView = new JMenuItem(MessageCatalog.BROWSER_SIMPLE_VIEW.getMessage());
		simpleView.setMnemonic(MessageCatalog.BROWSER_SIMPLE_VIEW_HK.getChar());
		simpleView.setSelected(true);
		simpleView.addActionListener(e -> simpleView());

		var fullView = new JMenuItem(MessageCatalog.BROWSER_FULL_VIEW.getMessage());
		fullView.setMnemonic(MessageCatalog.BROWSER_FULL_VIEW_HK.getChar());
		fullView.setSelected(false);
		fullView.addActionListener(e -> fullView());

		mnColumnMenu.add(simpleView);
		mnColumnMenu.add(fullView);
		mnColumnMenu.addSeparator();

		var len = ColumnsId.values().length;
		arColumnsMenuItem = new JCheckBoxMenuItem[len];

		/*
		 * Creates menu entry for each column name. Sets the hot key for each menu entry
		 * avoiding the use of any previous used hot key.
		 */
		var alMnemonics = new ArrayList<Character>();
		alMnemonics.add(MessageCatalog.BROWSER_SIMPLE_VIEW_HK.getChar());
		alMnemonics.add(MessageCatalog.BROWSER_FULL_VIEW_HK.getChar());
		for (var cont = 0; cont < len; cont++) {
			arColumnsMenuItem[cont] = new JCheckBoxMenuItem();
			var text = ColumnsId.values()[cont].getText();
			arColumnsMenuItem[cont].setText(text);
			text = text.replaceAll(" ", "").replaceAll("/.", ""); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			var nameLength = text.length();
			var visible = preferences.getBoolean(ColumnsId.values()[cont].name(), SIMPLE_VIEW[cont]);
			arColumnsMenuItem[cont].setSelected(visible);
			var found = false;
			for (var cha = 0; !found && cha < nameLength; cha++) {
				var caracter = text.charAt(cha);
				if (!alMnemonics.contains(caracter)) {
					found = true;
					alMnemonics.add(caracter);
					arColumnsMenuItem[cont].setMnemonic(caracter);
				}
			}
			arColumnsMenuItem[cont].addActionListener(e -> updateColumnVisibility());
			mnColumnMenu.add(arColumnsMenuItem[cont]);
		}

		updateColumnVisibility();
	}

	/**
	 * Modify visualization status using the "simple view".
	 */
	private void simpleView() {

		for (var cont = 0; cont < SIMPLE_VIEW.length; cont++) {
			arColumnsMenuItem[cont].setSelected(SIMPLE_VIEW[cont]);
		}

		updateColumnVisibility();
	}

	/**
	 * Modify visualization status showing all available columns.
	 */
	private void fullView() {
		var len = arColumnsMenuItem.length;
		for (var cont = 0; cont < len; cont++) {
			arColumnsMenuItem[cont].setSelected(true);
		}

		updateColumnVisibility();
	}

	/**
	 * Update column visibility status according to options marked on menu.
	 */
	private void updateColumnVisibility() {
		var len = arColumnsMenuItem.length;
		var arVisibility = new boolean[len];
		for (var cont = 0; cont < len; cont++) {
			var visible = arColumnsMenuItem[cont].isSelected();
			arVisibility[cont] = visible;
			preferences.putBoolean(ColumnsId.values()[cont].name(), visible);
		}

		tableModel.setVisible(arVisibility);
	}
}
