/*
 * Copyright 2024 Redeia.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTIBIILTY or FITNESS FOR A PARTICULAR PURPOSE. See GNU Lesser General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see
 * http://www.gnu.org/licenses/.
 *
 * Any redistribution and/or modification of this program has to make
 * reference to Redeia as the copyright owner of the program.
 */

package es.ree.eemws.core.utils.i18n;

/**
 * Message codes for utils.
 *
 * @author Redeia.
 * @version 2.1 01/01/2024
 */
public enum MessageCatalog {

	/** Unable to retrieve remote user from the https context [IP=?]. */
	HAND_001,

	/** Message is not valid against schema. Details: ?. */
	HAND_002,

	/** User has no proper role for current message type. */
	HAND_003, 

	/** Unable to read soap body. */
	HAND_004,

	/** Unsupported combination: [verb=?][noun=?] */
	HAND_005,

	/** Invalid message format */
	HAND_006,

	/** Invalid signature. */
	HAND_007,

	/** Signature syntax error. */
	HAND_008,

	/** Unable to sign message. */
	HAND_009,

	/** Server returns FAULT message. */
	HAND_010,

	/** Response message has no payload or payload is empty. */
	HAND_011,

	/** Response has invalid header elements: [verb=?][noun=?] expected: [verb=?][noun=?] */
	HAND_012,

	/** Client do not trust server identity. Check client's trust store, check URL (do not use IP). */
	HAND_013,

	/** The identity of the server certificate does not match the configured URL. */
	HAND_014,

	/** The configured URL is unknown and cannot connected to it. Check URL, check DNS. */
	HAND_015,

	/** The requested service does not exist on the server. Check configured URL. (HTTP=404, Not found). */
	HAND_016,

	/** You do not have permission to access the web services on the server. (HTTP=403, Forbidden). */
	HAND_017,

	/** You are unauthorized to perform such request. (HTTP=401, Unauthorized). */
	HAND_018,

	/** The server has received your request, but didn't sent a response in return (HTTP=200, Accepted). */
	HAND_019,

	/** The server has refused the connection. Check connectivity settings, check firewall settings. */
	HAND_020,

	/** The server has rejected the request. (HTTP=400, Bad request) */
	HAND_021,

	/** Runtime exception. Check application logs. */
	HAND_022,

	/** Server has returned an unrecognized name. Disable SNI extension and retry. */
	HAND_023,

	/** Invalid parameters. Code must be a positive integer value. */
	LST_001,

	/** Invalid operation parameters. Code must be an integer value. */
	LST_002,

	/** Invalid operation parameters. EndTime cannot precede StartTime. */
	LST_003,

	/** Invalid operation parameters. Time interval cannot span more than ? days. */
	LST_004,

	/** Invalid operation parameters. You must provide either Code or StartTime and EndTime time interval values */
	LST_005,

	/** Database read failed. */
	LST_006,

	/** The operation returns more than ? messages. Please, use a smaller time interval value or add more filters. */
	LST_007,

	/** Unable to create list response. */
	LST_008,

	/** Invalid operation parameters. IntervalType must be one of: ? */
	LST_009,

	/** Invalid operation parameters. ? */
	LST_010,

	/** Unknown parameter for list operation: ? */
	LST_011,

	/** Received message list has an invalid entry with no message code. */
	LST_012,

	/** Received message list has an invalid list entry with no message identification. Message info: [code=?]. */
	LST_013,

	/**
	 * Received message list has an invalid list entry with no message type. Message info: [code=?][id=?].
	 */
	LST_014,

	/** Received message list has an invalid list entry with no start application time interval. Message info: [code=?][id=?][type=?]. */
	LST_015,

	/** Received message list has an invalid list entry with no server timestamp. Message info: [code=?][id=?][type=?].	 */
	LST_016,

	/** Received message list has an invalid list entry with no owner. Message info: [code=?][id=?][type=?]. */
	LST_017,

	/** Received message list is invalid. ? */
	LST_018,

	/** Unable to get message list from the received payload. */
	LST_019,

	/** User has exceeded list operation limits. User is temporarily blocked. */
	LST_020,

	/** Invalid operation parameters. Code must be a positive integer value. */
	GET_001,

	/** Invalid operation parameters. Code must be an integer value. */
	GET_002,

	/** Invalid invocation parameters. You must provide either Code or MessageIdentification and MessageVersion values.	 */
	GET_003,

	/**	Invalid invocation parameters. You must provide Code or MessageIdentification and MessageVersion values. */
	GET_004,

	/** QUEUE filter is not supported. */
	GET_005,

	/** The requested message doesn't exist. */
	GET_006,

	/** Database read failed. */
	GET_007,

	/** Unable to create get response. */
	GET_008,

	/** Unable to filter the message payload. */
	GET_009,

	/** User has exceeded get operation limits. User is temporarily blocked. */
	GET_010,

	/** Invalid operation parameters. ? */
	GET_011,

	/** Unknown parameter for get operation: ? */
	GET_012,

	/** File read failed. */
	GET_013,

	/** The received message format is not supported [?]. Valid formats are [?] */
	GET_014,

	/** Unable to unzip received binary XML. */
	GET_015,

	/** Unable to read payload element. */
	GET_016,

	/** Queue value must be "?" not "?" */
	GET_017,

	/** Server returns a binary message but did not provide file name. */
	GET_018,

	/** MessageVersion must be a positive integer. */
	GET_019,

	/** Server requires MessageVersion when MessageIdentification is provided. */
	GET_020,

	/** The received message's size ? is greater that the maximum allowed ? */
	GET_021,

	/** Remote system is unable to process your message [?] and cannot give a detailed (human readable) reason why. Please ask system administrator. */
	PUT_001,

	/** System is currently processing a message for the same message type and application date. Please wait until the system provides a proper acknowledgement. */
	PUT_002,

	/** Malformed request: [provided noun=?] [expected noun=?] */
	PUT_003,

	/** Unable to process message [?] there is no message handler for [noun=?]. Check your client's URL. */
	PUT_004,

	/** You have no rights on this message type [MsgType=?][Rights=?] */
	PUT_005,

	/** Invalid signature. Details: ? */
	PUT_006,

	/** The provided signature document is incorrect and cannot be validated. Details: ? */
	PUT_007,

	/** There is no relationship among the identities [Sender=?][Signer=?][Owner=?] */
	PUT_008,

	/** Unable to store the message. Please, send a new document version. If the message persists, ask the system administrator. */
	PUT_009,

	/** Invalid message format: ? */
	PUT_010,

	/** Invalid binary payload */
	PUT_011,

	/** Message was rejected due to technical validations: ? */
	PUT_012,

	/** Received message has no payload or it is empty. */
	PUT_013,

	/** Cannot create request: ? */
	PUT_014,

	/** Cannot process response: ? */
	PUT_015,

	/** User has exceeded put operation limits. User is temporarily blocked. */
	PUT_016,

	/** Invalid operation parameters. ? */
	PUT_017,

	/** The received message's size ? is greater that the maximum allowed ? */
	PUT_018,

	/** Invalid parameters. DataType value must be provided. */
	QRY_001,

	/** Invalid parameters. Provided DataType value (?) is not recognized. */
	QRY_002,

	/** Invalid parameters. EndTime cannot precede StartTime. */
	QRY_003,

	/** Unable to create QueryData response. */
	QRY_004,

	/** Invalid parameters. Provided value ? for parameter ? is not recognized. */
	QRY_005,

	/** Database read failed. */
	QRY_006,

	/** Invalid parameters. Provided value ? for parameter ? must be a positive number. */
	QRY_007,

	/** The given parameters (?, ?) are mutually exclusive. */
	QRY_008,

	/** Provided date parameter ? has invalid format: ? */
	QRY_009,

	/** Invalid operation parameters. ? */
	QRY_010,

	/** Unknown parameter for query DataType ?: ? */
	QRY_011,

	/** Cannot process response: ? */
	QRY_012,

	/** User has exceeded Query operation limits. User is temporarily blocked. */
	QRY_013,
	
	/** Empty noun tag. */
	NO_NOUN_EXPECTED, 
	
	/** Invalid message. The received message has no header element. */
	NO_HEADER,
	
	/** Unable to read the given config file [{0}] */
	CONFIG_UNABLE_TO_READ_CONFIG_FILE,

	/** Unable to cipher the key [{0}] */
	CONFIG_UNABLE_TO_CIPHER_PASSWORD,

	/** Unable to save the configuration file. */
	CONFIG_UNABLE_TO_SAVE_CONFIG,

	/** Unable to decrypt value for key [{0}]. Edit the configuration file and set its value as clear text. */
	CONFIG_UNABLE_TO_DECRYPT_PASSWORD,

	/** Unable to read the key store [{0}]. */
	CONFIG_UNABLE_TO_READ_KEY_STORE,

	/** Unable to load the keystore [{0}]. Check keystore password and keystore type. */
	CONFIG_UNABLE_TO_LOAD_KEY_STORE,

	/** Unable to cipher the given value. */
	SECURITY_UNABLE_TO_CIPHER,

	/** Unable to decrypt the given value. */
	SECURITY_UNABLE_TO_DECRYPT,

	/** Null key info! */
	SECURITY_NULL_KEY_INFO,

	/** No valid KeyValue element found! */
	SECURITY_NO_VALID_KEY,

	/** Signature validation failed. */
	SECURITY_SIGNATURE_VALIDATION_FAILED,

	/** The certificate signature is not valid [Today={0,date,dd/MM/yyyy}][{1,date,dd/MM/yyyy}-{2,date,dd/MM/yyyy}] */
	SECURITY_SIGNATURE_CERTIFICATE_NOT_VALID,

	/** The certificate signature is not trusted. */
	SECURITY_SIGNATURE_NO_TRUSTED_CERT,

	/** Unable to verify signature */
	SECURITY_UNABLE_TO_VERIFY,

	/** The system's key store is not set. Set the system property [{0}] */
	SECURITY_NO_SYSTEM_KEY_STORE,

	/** Unable to find a usable certificate in the set key store. */
	SECURITY_NO_USABLE_CERTIFICATE_FOUND,

	/** Unable to read the specified system key store [{0}]. Check system parameter [{1}], check file read permission */
	SECURITY_UNABLE_TO_READ_KEY_STORE,

	/** Unable to load the specified system key store [{0}]. Check parameters [{1}, {2}]] */
	SECURITY_UNABLE_TO_LOAD_KEY_STORE,

	/** The given document seems to be an invalid XML. */
	SECURITY_INVALID_DOCUMENT,

	/** Invalid document. The given document has no [{0}:{1}] tag element to read / write the signature. */
	SECURITY_INVALID_DOCUMENT_NO_HEADER,

	/** Invalid signature algorithm / parameters. */
	SECURITY_INVALID_GENERAL_ERROR,

	/** Unable to sign the given document. Check document and exception details. */
	SECURITY_INVALID_DOCUMENT_CANNOT_SIGN,

	/** [Ref {0}=[{1}]] */
	SECURITY_REFERENCE_STATUS,

	/** [Valid=YES] */
	SECURITY_REFERENCE_STATUS_VALID,

	/** [Valid=NO][Provided Digest={0}][Expected Digest={1}] */
	SECURITY_REFERENCE_STATUS_NO_VALID,

	/** [Signature valid=YES] */
	SECURITY_SIGNATURE_STATUS_VALID,

	/** [Signature valid=NO] */
	SECURITY_SIGNATURE_STATUS_NO_VALID,

	/** The provided signature document is incorrect and cannot be validated. */
	SECURITY_SIGNATURE_SYNTAX_ERROR,

	/** [Certificate valid=YES] */
	SECURITY_CERTIFICATE_STATUS_VALID,

	/** [Certificate valid=NO] */
	SECURITY_CERTIFICATE_STATUS_NO_VALID,

	/** Unable to find an X509 Trust validator */
	SECURITY_NO_TRUST_VALIDATOR,

	/** Unable to initialize the X509 Trust validator [{0}] */
	SECURITY_UNABLE_TO_INITIALIZE_TRUST_VALIDATOR,

	/** Error creating XMLGregorianCalendarFactory */
	XML_GREGORIAN_CALENDAR,

	/** Unable to retrieve message form the given context. */
	IEC_UNABLE_TO_RETRIEVE_FROM_CONTEXT,

	/** The provider date parameter {0} has an invalid value or format. */
	INVALID_DATE_PARAMETER_VALUE,

	/** Parameter name {0} was specified at least twice. */
	INVALID_PARAMETER_TWICE,

	/** Received message has no Request element or it is empty. */
	REQUEST_MESSAGE_HAS_NO_REQUEST,

	/** Cannot return a binary response as string! */
	RESPONSE_IS_NOT_XML,
	
	/** Cannot read the received SOAP message. */
	SOAP_UNABLE_TO_TRANSFORM;

	/**
	 * Gets message text.
	 * 
	 * @return Message text.
	 */
	public String getMessage(final Object... parameters) {
		return Messages.getString(name(), parameters);
	}

}