/*
 * Copyright 2024 Redeia.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTIBIILTY or FITNESS FOR A PARTICULAR PURPOSE. See GNU Lesser General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see
 * http://www.gnu.org/licenses/.
 *
 * Any redistribution and/or modification of this program has to make
 * reference to Redeia as the copyright owner of the program.
 */

package es.ree.eemws.kit.folders;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <code>LockHandler</code> class implements the remote interface to communicate
 * working group.
 *
 * @author Redeia.
 * @version 2.1 01/01/2024
 *
 */
public final class LockHandler extends UnicastRemoteObject implements LockHandlerIntf {

	/** Class ID. */
	private static final long serialVersionUID = 9069481093242188767L;

	/** Constant for "only one server in the group. */
	private static final int ONLY_ONE_SERVER = 1;

	/** Number of invalid attempts before give up. */
	private static final int NUM_RETRIES = 3;

	/** Number of milliseconds to wait before retry after a failed attempt. */
	private static final long SLEEP_BEFORE_RETRY = 5000;

	/** Indicate whether is there an only server or multiple servers. */
	private boolean single = false;

	/** Names of locked files. */
	private List<String> lockFiles;

	/** Names of messages to be locked. */
	private List<String> tryLockFiles;

	/** Unique ID for this service in the group. */
	private int thisServiceID;

	/** Members of the group. */
	private ArrayList<Member> members = new ArrayList<>();

	/** This server RMI URL reference. */
	private String thisServerURL;

	/** Logging system. */
	private static final Logger LOGGER = Logger.getLogger(LockHandler.class.getName());

	/**
	 * Constructor. Creates a new lock manager according to the configuration. The
	 * manager will find the remote references to all members and will subscribe to
	 * all of them.
	 *
	 * @param config System settings.
	 * @throws RemoteException If cannot create an RMI register and subscribe to it.
	 */
	public LockHandler(final MagicFolderConfiguration config) throws RemoteException {
		var serviceID = config.getRmiServiceNumber();
		var membersRmiUrls = config.getMembersRmiUrls();

		if (serviceID == null || membersRmiUrls == null || membersRmiUrls.size() == ONLY_ONE_SERVER) {
			single = true;
			if (LOGGER.isLoggable(Level.INFO)) {
				LOGGER.info(MessageCatalog.MF_STAND_ALONE.getMessage());
			}
		} else {

			single = false;
			lockFiles = new ArrayList<>();
			tryLockFiles = new ArrayList<>();

			thisServiceID = Integer.parseInt(serviceID);
			thisServerURL = membersRmiUrls.get(thisServiceID - 1);
			bindToRMIRegistry(thisServerURL);
			membersRmiUrls.remove(thisServerURL);

			if (LOGGER.isLoggable(Level.INFO)) {
				LOGGER.info(MessageCatalog.MF_SEARCHING_MEMBERS.getMessage(String.valueOf(membersRmiUrls.size())));
			}

			getRemoteReferences(membersRmiUrls, thisServerURL);

			if (LOGGER.isLoggable(Level.INFO)) {
				LOGGER.info(MessageCatalog.MF_GROUP_LISTEINGN_URL.getMessage(thisServerURL));
			}
		}
	}

	@Override
	public boolean equals(Object otherObject) {
		var retValue = false;
		if (otherObject instanceof LockHandler) {

			var other = (LockHandler) otherObject;

			retValue = single && other.single
			        || !single && !other.single && thisServerURL.equalsIgnoreCase(other.thisServerURL);

		}

		return retValue;
	}

	@Override
	public int hashCode() {
		int retValue;
		if (single) {
			retValue = 1;
		} else {
			retValue = thisServerURL.hashCode();
		}

		return retValue;
	}

	/**
	 * Creates an RMI registry on port passed as argument and bind the current
	 * object to it..
	 *
	 * @param url URL where service subscribes.
	 * @throws RemoteException If cannot connect to RMI server.
	 */
	private void bindToRMIRegistry(final String url) throws RemoteException {

		try {
			createRMIRegistry(url);
			Naming.rebind(url, this);
		} catch (MalformedURLException e) {
			throw new RemoteException(MessageCatalog.MF_INVALID_HOST_PORT.getMessage(url));
		}
	}

	/**
	 * Creates a RMI registry with the given URL.
	 *
	 * @param url URL where service subscribes.
	 */
	private void createRMIRegistry(final String url) {
		try {
			/* Search port in URL expression: rmi://<host>:<port>/service_name */
			var colonPosition = url.indexOf(":"); //$NON-NLS-1$
			colonPosition = url.indexOf(":", colonPosition + 1); //$NON-NLS-1$
			var port = url.substring(colonPosition + 1, url.indexOf("/", colonPosition)); //$NON-NLS-1$
			LocateRegistry.createRegistry(Integer.parseInt(port));
		} catch (RemoteException ex) {
			if (LOGGER.isLoggable(Level.WARNING)) {
				LOGGER.warning(MessageCatalog.MF_UNABLE_TO_CREATE_REGISTRY.getMessage());
			}
		}
	}

	/**
	 * Retrieve remote references to neighbors.
	 *
	 * @param membersRmiUrls List of all members' URL.
	 * @param thisMemberUrl  This member URL.
	 * @throws RemoteException If the settings of any server are incorrect.
	 */
	private void getRemoteReferences(final List<String> membersRmiUrls, final String thisMemberUrl)
	        throws RemoteException {

		boolean gotReference;

		for (String memberUrl : membersRmiUrls) {

			gotReference = false;

			if (LOGGER.isLoggable(Level.INFO)) {
				LOGGER.info(MessageCatalog.MF_CONNECTING_WITH_MEMBER.getMessage(memberUrl));
			}

			for (var times = 0; !gotReference && times < NUM_RETRIES; times++) {

				try {
					var interfaceN = (LockHandlerIntf) Naming.lookup(memberUrl);

					interfaceN.suscribe(thisMemberUrl);

					var mem = new Member(memberUrl, interfaceN);

					synchronized (members) {
						if (!members.contains(mem)) {
							members.add(mem);
						}
					}

					gotReference = true;
				} catch (MalformedURLException e) {
					throw new RemoteException(MessageCatalog.MF_INVALID_MEMBER_CONFIGURATION.getMessage(memberUrl));
				} catch (RemoteException | NotBoundException ex) {
					if (LOGGER.isLoggable(Level.INFO)) {
						LOGGER.info(MessageCatalog.MF_MEMBER_NOT_AVAILABLE_YET.getMessage(memberUrl));
					}
				}

				try {
					Thread.sleep(SLEEP_BEFORE_RETRY * (times + 1));
				} catch (InterruptedException ex) {
					LOGGER.finest("Interrupted!"); // Don't mind! //$NON-NLS-1$
					Thread.currentThread().interrupt();
				}
			}

			if (!gotReference && LOGGER.isLoggable(Level.WARNING)) {
				LOGGER.warning(MessageCatalog.MF_MEMBER_NOT_AVAILABLE.getMessage(memberUrl));
			}
		}
	}

	/**
	 * Release the message passed as argument in order to can be retrieved by other
	 * members.
	 *
	 * @param fileName ID of the message to be released.
	 */
	public void releaseLock(final String fileName) {
		if (!single) {
			synchronized (lockFiles) {
				lockFiles.remove(fileName);
			}
		}
	}

	/**
	 * Indicate to this server the existence of a new member in the group.
	 *
	 * @param remoteURL remote URL (rmi://host:port/service) of the new member.
	 */
	@Override
	public void suscribe(final String remoteURL) {

		try {
			var remoteReference = (LockHandlerIntf) Naming.lookup(remoteURL);

			synchronized (members) {
				var updated = false;
				for (Member member : members) {
					if (remoteURL.equals(member.getUrl())) {
						if (LOGGER.isLoggable(Level.INFO)) {
							LOGGER.info(MessageCatalog.MF_UPDATE_MEMBER.getMessage(remoteURL));
						}
						member.setRemoteReference(remoteReference);
						updated = true;
					}
				}

				/* New member. */
				if (!updated) {
					if (LOGGER.isLoggable(Level.INFO)) {
						LOGGER.info(MessageCatalog.MF_NEW_MEMBER.getMessage(remoteURL));
					}
					members.add(new Member(remoteURL, remoteReference));
				}
			}
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			if (LOGGER.isLoggable(Level.WARNING)) {
				LOGGER.warning(MessageCatalog.MF_INVALID_URL_RECEIVED.getMessage(remoteURL));
			}
		}
	}

	/**
	 * Try to lock the message passed as argument.
	 *
	 * @param fileName Name of the message to lock.
	 * @return <code>true</code> If the file could be locked <code>false</code>
	 *         otherwise.
	 */
	public boolean tryLock(final String fileName) {
		var canLock = true;

		if (!single) {
			synchronized (lockFiles) {
				tryLockFiles.add(fileName);
			}

			var lockedByNeighbor = isLockedByNeighbor(fileName);

			synchronized (lockFiles) {
				if (!lockedByNeighbor) {
					if (tryLockFiles.contains(fileName)) {
						canLock = true;
						lockFiles.add(fileName);
					} else {
						canLock = false;
					}
				} else {
					canLock = false;
				}

				tryLockFiles.remove(fileName);
			}
		}

		return canLock;
	}

	/**
	 * Ask neighbors (other group members) whether the file is locked. If any
	 * communication problem is found (eg. crashed), is removed from group.
	 *
	 * @param fileName file to query.
	 * @return <code>true</code> If the file is locked by another member.
	 *         <code>false</code> Otherwise.
	 */
	private boolean isLockedByNeighbor(final String fileName) {
		var lockedByNeighbor = false;

		var count = 0;

		var numberOfMembers = members.size();

		while (!lockedByNeighbor && (count < numberOfMembers)) {
			var interfaceN = members.get(count).getRemoteReference();
			var success = false;

			for (var attempt = 0; !success && (attempt < NUM_RETRIES); attempt++) {
				try {
					lockedByNeighbor = interfaceN.isLocked(fileName, thisServiceID);
					count++;
					success = true;
				} catch (RemoteException ex) {
					if (LOGGER.isLoggable(Level.WARNING)) {
						LOGGER.warning(MessageCatalog.MF_MEMBER_NOT_AVAILABLE.getMessage(members.get(count).getUrl()));
					}
					try {
						Thread.sleep(SLEEP_BEFORE_RETRY);
					} catch (InterruptedException ex1) {
						LOGGER.fine("Interrupted!"); //$NON-NLS-1$
						Thread.currentThread().interrupt();
					}
				}
			}

			/*
			 * If after multiple attempts node does not response is tagged as 'dead' and
			 * will no be communicated any longer until it is not be subscribed again.
			 */
			if (!success) {
				synchronized (members) {
					if (LOGGER.isLoggable(Level.SEVERE)) {
						LOGGER.severe(MessageCatalog.MF_MEMBER_GONE.getMessage(members.get(count).getUrl()));
					}
					members.remove(count);
					numberOfMembers--;
				}
			}
		}

		return lockedByNeighbor;
	}

	/**
	 * Check whether the message passed as argument is locked by this member. A
	 * message is locked if: - Is in the list of Locked messages. - Is in the list
	 * of desired messages and this server has bigger priority (lower remote ID).
	 *
	 * @param fileName Name of the message to retrieve.
	 * @param remoteID Remote ID of the asking server.
	 * @return <code>true</code> if the message is locked by this member
	 *         <code>false</code> otherwise.
	 * @throws RemoteException If there is no response from member.
	 */
	@Override
	public boolean isLocked(final String fileName, final int remoteID) throws RemoteException {
		var retValue = false;

		synchronized (lockFiles) {

			retValue = (lockFiles.contains(fileName) || (tryLockFiles.contains(fileName) && thisServiceID < remoteID));

			if (tryLockFiles.contains(fileName) && thisServiceID > remoteID) {
				tryLockFiles.remove(fileName);
			}
		}

		return retValue;
	}

	/**
	 * Stores information about working group members.
	 */
	private class Member implements Serializable {

		/** Serial version UID. */
		private static final long serialVersionUID = 6364456351425329956L;

		/** RMI URL to access to a member. */
		private String rmiUrl;

		/** Remote RMI reference. */
		private LockHandlerIntf remoteReference;

		/**
		 * Creates a new member given its URL and remote interface.
		 *
		 * @param url      URL of the member.
		 * @param remIterf Remote interface.
		 */
		Member(final String url, final LockHandlerIntf remIterf) {
			rmiUrl = url;
			remoteReference = remIterf;
		}

		/**
		 * Sets the remote interface of a member (update).
		 *
		 * @param remoteRef Remote interface of a member.
		 */
		public void setRemoteReference(final LockHandlerIntf remoteRef) {
			remoteReference = remoteRef;
		}

		/**
		 * Get the remote interface of a member.
		 *
		 * @return RMI url of the member.
		 */
		public String getUrl() {
			return rmiUrl;
		}

		/**
		 * Return the remote interface of the member.
		 *
		 * @return Remote interface.
		 */
		public LockHandlerIntf getRemoteReference() {
			return remoteReference;
		}
	}
}
