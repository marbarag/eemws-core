/*
 * Copyright 2024 Redeia.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTIBIILTY or FITNESS FOR A PARTICULAR PURPOSE. See GNU Lesser General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see
 * http://www.gnu.org/licenses/.
 *
 * Any redistribution and/or modification of this program has to make
 * reference to Redeia as the copyright owner of the program.
 */

package es.ree.eemws.core.utils.file;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;


/**
 * Utilities to compress and decompress with gzip.
 *
 * @author Redeia.
 * @version 2.1 01/01/2024
 */
public final class GZIPUtil {

    /** Buffer size in bytes. */
    private static final int BUFFER_SIZE = 1024;

    /**
     * Constructor.
     */
    private GZIPUtil() {

        /* This method should not be implemented. */
    }

    /**
     * Compress the data with gzip.
     * @param dataToCompress Data to compress.
     * @return Data compress.
     * @throws IOException Exception with the error.
     */
    public static byte[] compress(final byte[] dataToCompress) throws IOException {

        ByteArrayOutputStream byteStream = new ByteArrayOutputStream(dataToCompress.length);
        try (GZIPOutputStream zipStream = new GZIPOutputStream(byteStream);) {

            zipStream.write(dataToCompress);
        }

        return byteStream.toByteArray();
    }

    /**
     * Uncompress the data with gzip.
     * @param contentBytes Data to uncompress.
     * @return Data uncompress.
     * @throws IOException Exception with the error.
     */
    public static byte[] uncompress(final byte[] contentBytes) throws IOException {

        try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {

            try (GZIPInputStream gzis = new GZIPInputStream(new ByteArrayInputStream(contentBytes));) {

                int len;
                byte[] buffer = new byte[BUFFER_SIZE];
                while ((len = gzis.read(buffer)) > 0) {

                    out.write(buffer, 0, len);
                }
            }

            return out.toByteArray();
        }
    }
}
