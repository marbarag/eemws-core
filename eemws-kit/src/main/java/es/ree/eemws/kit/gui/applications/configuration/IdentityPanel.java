/*
 * Copyright 2024 Redeia.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTIBIILTY or FITNESS FOR A PARTICULAR PURPOSE. See GNU Lesser General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see
 * http://www.gnu.org/licenses/.
 *
 * Any redistribution and/or modification of this program has to make
 * reference to Redeia as the copyright owner of the program.
 */

package es.ree.eemws.kit.gui.applications.configuration;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateKey;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import es.ree.eemws.core.utils.config.ConfigException;
import es.ree.eemws.core.utils.security.CryptoException;
import es.ree.eemws.core.utils.security.CryptoManager;
import es.ree.eemws.kit.config.Configuration;

/**
 * Configuration's identity panel.
 *
 * @author Redeia.
 * @version 2.1 01/01/2024
 */
public final class IdentityPanel extends JPanel {

	/** Class ID. */
	private static final long serialVersionUID = 843541923336031179L;

	/** PKCS12 store type. */
	private static final String PKCS12_STORE_TYPE = "PKCS12";
	
	/** Dialogue to select certificate. */
	private JFileChooser certFileChooser;

	/** Text box display the path to Certificate File . */
	private JTextField certFile;

	/** Password to access certificate. */
	private JPasswordField certPassword1;

	/** Confirm password to access certificate. */
	private JPasswordField certPassword2;

	/**
	 * Constructor. Creates a new panel with all the elements to setup the client
	 * certificate.
	 */
	public IdentityPanel() {

		certFile = new JTextField();
		certFile.setText(""); //$NON-NLS-1$
		certFile.setBounds(new Rectangle(154, 65, 204, 20));

		var fileLbl = new JLabel();
		fileLbl.setDoubleBuffered(false);
		fileLbl.setRequestFocusEnabled(true);
		fileLbl.setText(MessageCatalog.SETTINGS_IDENTITY_CERTIFICATE_FILE.getMessage());
		fileLbl.setDisplayedMnemonic(MessageCatalog.SETTINGS_IDENTITY_CERTIFICATE_FILE_HK.getChar());
		fileLbl.setHorizontalAlignment(SwingConstants.RIGHT);
		fileLbl.setLabelFor(certFile);
		fileLbl.setBounds(new Rectangle(17, 63, 134, 21));

		certPassword1 = new JPasswordField();
		certPassword1.setText(""); //$NON-NLS-1$
		certPassword1.setBounds(new Rectangle(154, 93, 204, 20));

		var passLbl = new JLabel();
		passLbl.setText(MessageCatalog.SETTINGS_IDENTITY_CERTIFICATE_PASSWORD.getMessage());
		passLbl.setDisplayedMnemonic(MessageCatalog.SETTINGS_IDENTITY_CERTIFICATE_PASSWORD_HK.getChar());
		passLbl.setHorizontalAlignment(SwingConstants.RIGHT);
		passLbl.setLabelFor(certPassword1);
		passLbl.setBounds(new Rectangle(17, 92, 129, 21));

		certPassword2 = new JPasswordField();
		certPassword2.setBounds(new Rectangle(154, 121, 204, 20));
		certPassword2.setText(""); //$NON-NLS-1$

		var pass2Lbl = new JLabel();
		pass2Lbl.setBounds(new Rectangle(17, 121, 129, 17));
		pass2Lbl.setText(MessageCatalog.SETTINGS_IDENTITY_CERTIFICATE_PASSWORD2.getMessage());
		pass2Lbl.setDisplayedMnemonic(MessageCatalog.SETTINGS_IDENTITY_CERTIFICATE_PASSWORD2_HK.getChar());
		pass2Lbl.setHorizontalAlignment(SwingConstants.RIGHT);
		pass2Lbl.setLabelFor(certPassword2);

		var examine = new JButton();
		examine.setBounds(new Rectangle(369, 60, 110, 26));
		examine.setText(MessageCatalog.SETTINGS_IDENTITY_CERTIFICATE_BROWSE.getMessage());
		examine.setMnemonic(MessageCatalog.SETTINGS_IDENTITY_CERTIFICATE_BROWSE_HK.getChar());
		examine.addActionListener(e -> findCertificate());

		var dataLbl = new JLabel();
		dataLbl.setText(MessageCatalog.SETTINGS_IDENTITY_CERTIFICATE_DATA.getMessage());
		dataLbl.setBounds(new Rectangle(18, 35, 418, 16));

		certFileChooser = new JFileChooser();

		setMinimumSize(new Dimension(1, 1));
		setLayout(null);
		add(certFileChooser, null);
		add(certPassword1, null);
		add(fileLbl, null);
		add(passLbl, null);
		add(pass2Lbl, null);
		add(certPassword2, null);
		add(certFile, null);
		add(examine, null);
	}

	/**
	 * Load certificate settings into form.
	 *
	 * @param cm Configuration object from which values are read.
	 */
	public void loadValues(final Configuration cm) {

		String value;
		value = cm.getKeyStoreFile();
		if (value == null) {
			value = ""; //$NON-NLS-1$
		}
		certFile.setText(value);

		value = cm.getKeyStorePassword();
		if (value == null) {
			value = ""; //$NON-NLS-1$
		}
		certPassword1.setText(value);
		certPassword2.setText(value);
	}

	/**
	 * Open file chooser to select path to certificate file.
	 */
	private void findCertificate() {

		if (certFileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {

			var file = certFileChooser.getSelectedFile();
			if (!file.exists()) {

				JOptionPane.showMessageDialog(this, MessageCatalog.SETTINGS_IDENTITY_FILE_DOESNT_EXISTS.getMessage(),
				        MessageCatalog.SETTINGS_IDENTITY_FILE_DOESNT_EXISTS.getMessage(), JOptionPane.ERROR_MESSAGE);

			} else {

				var path = file.getAbsolutePath();
				path = path.replaceAll("\\\\", "/"); //$NON-NLS-1$ //$NON-NLS-2$
				certFile.setText(path);
			}
		}
	}

	/**
	 * Validate certificate panel.
	 *
	 * @throws ConfigException If cannot access the certificate.
	 */
	public void validateConfig() throws ConfigException {

		var certFileStr = certFile.getText().trim().replaceAll("\\\\", "/");
		certFile.setText(certFileStr);
		
		if (certFileStr.isEmpty()) {
			throw new ConfigException(getPanelName() + " " //$NON-NLS-1$
			        + MessageCatalog.SETTINGS_PANEL_SAYS.getMessage() + " " //$NON-NLS-1$
			        + MessageCatalog.SETTINGS_IDENTITY_MUST_PROVIDE_CERTIFICATE_FILE.getMessage());
		}

		var file = new File(certFileStr);
		if (!file.exists()) {

			throw new ConfigException(getPanelName() + " " //$NON-NLS-1$
			        + MessageCatalog.SETTINGS_PANEL_SAYS.getMessage() + " " //$NON-NLS-1$
			        + MessageCatalog.SETTINGS_IDENTITY_FILE_DOESNT_EXISTS.getMessage());

		}

		if (!file.canRead()) {
			throw new ConfigException(getPanelName() + " " //$NON-NLS-1$
			        + MessageCatalog.SETTINGS_PANEL_SAYS.getMessage() + " " //$NON-NLS-1$
			        + MessageCatalog.SETTINGS_IDENTITY_FILE_CANNOT_READ.getMessage());

		}
		var pass1 = new String(certPassword1.getPassword());
		var pass2 = new String(certPassword2.getPassword());

		if (!pass1.equals(pass2)) {
			throw new ConfigException(getPanelName() + " " //$NON-NLS-1$
			        + MessageCatalog.SETTINGS_PANEL_SAYS.getMessage() + " " //$NON-NLS-1$
			        + MessageCatalog.SETTINGS_IDENTITY_PASSWORD_MATCH.getMessage());
		}

		try (var certFileIS = new FileInputStream(certFile.getText())) {
			var ks = KeyStore.getInstance(PKCS12_STORE_TYPE);

			ks.load(certFileIS, pass1.toCharArray());

			var keyAlias = ks.aliases();
			String entryAlias = null;
			var okAlias = false;

			while (!okAlias && keyAlias.hasMoreElements()) {

				entryAlias = keyAlias.nextElement();

				okAlias = isOkCertificate(pass1, ks, entryAlias);
			}

			if (!okAlias) {
				throw new ConfigException(getPanelName() + " " //$NON-NLS-1$
				        + MessageCatalog.SETTINGS_PANEL_SAYS.getMessage() + " " //$NON-NLS-1$
				        + MessageCatalog.SETTINGS_IDENTITY_NO_USABLE_CERTIFICATE.getMessage());
			}

		} catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException e) {
			throw new ConfigException(getPanelName() + " " //$NON-NLS-1$
			        + MessageCatalog.SETTINGS_PANEL_SAYS.getMessage() + " " //$NON-NLS-1$
			        + MessageCatalog.SETTINGS_IDENTITY_CERTIFICATE_CANNOT_BE_READ.getMessage());
		}
	}

	/**
	 * Check if the certificate with the given alias is a valid (usable) entry.
	 *
	 * @param pass       Keystore password.
	 * @param ks         Keystore
	 * @param entryAlias Key alias
	 * @return <code>true</code> if the certificate can be used. <code>false</code>
	 *         otherwise.
	 */
	private boolean isOkCertificate(final String pass, final KeyStore ks, final String entryAlias) {
		var okAlias = false;
		try {
			var privateKey = (RSAPrivateKey) ks.getKey(entryAlias, pass.toCharArray());
			var certificate = (X509Certificate) ks.getCertificate(entryAlias);
			certificate.checkValidity();

			// do not use keystore entries with no private key (usually a CA certificate)
			okAlias = privateKey != null;

		} catch (CertificateException | UnrecoverableKeyException | KeyStoreException | NoSuchAlgorithmException e) {

			okAlias = false;
		}

		return okAlias;
	}

	/**
	 * Sets Certificate settings.
	 *
	 * @param config Configuration object containing certificate settings.
	 */
	public void setValues(final Configuration config) {

		config.setKeyStoreFile(certFile.getText());
		try {
			config.setKeyStorePassword(CryptoManager.encrypt(new String(certPassword1.getPassword())));
		} catch (CryptoException e) {
			config.setKeyStorePassword(new String(certPassword1.getPassword()));
		}
	}

	/**
	 * Return panel name.
	 *
	 * @return Panel name.
	 */
	public String getPanelName() {

		return MessageCatalog.SETTINGS_IDENTITY_TAB.getMessage();

	}
}
