/*
 * Copyright 2024 Redeia.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTIBIILTY or FITNESS FOR A PARTICULAR PURPOSE. See GNU Lesser General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see
 * http://www.gnu.org/licenses/.
 *
 * Any redistribution and/or modification of this program has to make
 * reference to Redeia as the copyright owner of the program.
 */

package es.ree.eemws.kit.cmd.put;

import es.ree.eemws.kit.common.Messages;

/**
 * Common message catalog for command line.
 *
 * @author Redeia.
 * @version 2.1 01/01/2024
 */
public enum PutMessageCatalog {

	/** Parameter "in". */
	PUT_PARAMETER_IN,

	/** Parameter "attachment". */
	PUT_PARAMETER_ATTACHMENT,

	/** No file to send was specified!. */
	PUT_PARAMETER_NO_INPUT_FILE,

	/** You should provide either {0} or {1} but not both. */
	PUT_PARAMETER_NO_TWO_KINDS_INPUT_FILE,

	/** Put usage. */
	PUT_USAGE;

	/**
	 * Gets message text.
	 * @return Message text.
	 */
	public String getMessage(final Object... parameters) {
		return Messages.getString(name(), parameters);
	}

	/**
	 * Return the first chart of the message.
	 * @return First chart of the message.
	 */
	public char getChar() {
		return Messages.getString(name()).charAt(0);
	}



}