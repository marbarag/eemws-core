@echo off
call commEnv.cmd
if NOT EXIST ../log (
	CD ..
	MKDIR log
	CD bin
) 

@REM Do not include argument "-Dinteractive" in no-interactive environments  (ie daemons, background process, etc.)
@REM Set the property "-DRESET_CODE" to set the list by code to 0 (fresh start)
@REM Set the property "-RESET_DATE" to set the list by date to a specific date expressed in epoch format (ms from 1/1/1970)
@REM Set the property "-DLIST_BY_DATE" to list by server timestamp instead of code. 
@REM Set the property "-DMF_RETRY_ATTEMPTS" to the number of retry attempts of a get operation in case the operation fails.

start javaw %MEM_ARGS% %JAVA_OPTIONS% %FILE_LOG% -Dinteractive es.ree.eemws.kit.folders.FolderManager
