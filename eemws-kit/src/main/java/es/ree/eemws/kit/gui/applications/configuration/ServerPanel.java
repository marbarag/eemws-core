/*
 * Copyright 2024 Redeia.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTIBIILTY or FITNESS FOR A PARTICULAR PURPOSE. See GNU Lesser General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see
 * http://www.gnu.org/licenses/.
 *
 * Any redistribution and/or modification of this program has to make
 * reference to Redeia as the copyright owner of the program.
 */

package es.ree.eemws.kit.gui.applications.configuration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import es.ree.eemws.core.utils.config.ConfigException;
import es.ree.eemws.kit.config.Configuration;

/**
 * Configuration's server panel.
 *
 * @author Redeia.
 * @version 2.1 01/01/2024
 */
public final class ServerPanel extends JPanel {

	/** Class ID. */
	private static final long serialVersionUID = -7563932190801169737L;

	/** Environment config file extension. */
	private static final String ENV_FILE_EXTENSION = ".env"; //$NON-NLS-1$
	
	/** Text field To display a non-preset URL. */
	private JTextField selectedUrl;

	/** Label for {@link #selectedUrl}. */
	private JLabel enterUrlLbl;

	/** A list with all the found environment configurations. */
	private transient List<EnvironmentConfig> listEnv;

	/** ComboBox with the list of configurations. */
	private JComboBox<Object> environmentList = new JComboBox<>();

	/** Environment description text. */
	private JTextArea environmentDescription = new JTextArea();

	/** Current configuration url. */
	private String currentUrl;

	/**
	 * Constructor.
	 */
	public ServerPanel() {

		readEnvironmentFiles();

		var panelSrv = new JPanel();
		panelSrv.setLayout(null);
		panelSrv.setBounds(30, 20, 450, 180);
		panelSrv.setBorder(new TitledBorder(
		        new EtchedBorder(EtchedBorder.RAISED, java.awt.Color.white, new java.awt.Color(142, 142, 142)), " " //$NON-NLS-1$
		                + MessageCatalog.SETTINGS_SERVER_DATA.getMessage() + " ")); //$NON-NLS-1$

		enterUrlLbl = new JLabel(MessageCatalog.SETTINGS_SERVER_URL.getMessage());
		enterUrlLbl.setLabelFor(selectedUrl);
		enterUrlLbl.setDisplayedMnemonic(MessageCatalog.SETTINGS_SERVER_URL_HK.getChar());
		enterUrlLbl.setBounds(25, 10, 400, 55);
		enterUrlLbl.setVisible(true);
		panelSrv.add(enterUrlLbl, null);

		selectedUrl = new JTextField();
		selectedUrl.setText(""); //$NON-NLS-1$
		selectedUrl.setBounds(25, 50, 400, 20);
		selectedUrl.setVisible(true);
		panelSrv.add(selectedUrl);

		if (!listEnv.isEmpty()) {

			selectedUrl.setVisible(false);
			selectedUrl.setBounds(25, 80, 400, 20);

			enterUrlLbl.setLabelFor(environmentList);

			var itr = listEnv.iterator();
			environmentList.addItem(MessageCatalog.SETTINGS_SERVER_ENVIRONMENT_OTHER.getMessage());
			while (itr.hasNext()) {
				environmentList.addItem(itr.next().getTitle());
			}

			environmentList.addActionListener(e -> comboBoxItemSelected());
			environmentList.setBounds(25, 50, 400, 20);
			environmentList.setSelectedIndex(0);
			environmentList.setVisible(true);
			panelSrv.add(environmentList);

			environmentDescription.setText(listEnv.get(0).getDescription());
			environmentDescription.setBounds(25, 70, 400, 90);
			environmentDescription.setBackground(enterUrlLbl.getBackground());
			environmentDescription.setLineWrap(true);
			environmentDescription.setWrapStyleWord(true);
			environmentDescription.setEditable(false);
			panelSrv.add(environmentDescription);

		}

		setLayout(null);
		setOpaque(true);
		add(panelSrv, null);

	}

	/**
	 * Load server settings into form.
	 *
	 * @param cm Configuration object from which values are read.
	 */
	public void loadValues(final Configuration cm) {

		var serviceURL = cm.getUrlEndPoint();

		if (serviceURL == null) {

			selectedUrl.setText(""); //$NON-NLS-1$

		} else {

			selectedUrl.setText(serviceURL.toString());

			var itr = listEnv.iterator();
			var found = false;
			for (var cont = 1; itr.hasNext() && !found; cont++) {
				var itmUrl = itr.next().getURL();
				if (itmUrl.equals(serviceURL)) {
					found = true;
					environmentList.setSelectedIndex(cont);
					comboBoxItemSelected();
				}
			}

			if (!found) {
				currentUrl = serviceURL.toString();
			}

		}
	}

	/**
	 * Sets Server connection settings.
	 *
	 * @param config Configuration object.
	 */
	public void setValues(final Configuration config) {

		var urlUriValue = selectedUrl.getText().trim();
		config.setUrl(urlUriValue);
	}

	/**
	 * Validate panel.
	 *
	 * @throws ConfigException If the URL (on text box) is incorrect.
	 */
	public void validateConfig() throws ConfigException {

		var val = selectedUrl.getText().trim();
		String errMsg = null;
		if (val.length() == 0) {

			errMsg = MessageCatalog.SETTINGS_SERVER_NO_URL.getMessage();

		} else if (!val.startsWith("https://")) { //$NON-NLS-1$

			errMsg = MessageCatalog.SETTINGS_SERVER_NO_HTTPS.getMessage();

		}

		if (errMsg != null) {
			throw new ConfigException(getPanelName() + " " //$NON-NLS-1$
			        + MessageCatalog.SETTINGS_PANEL_SAYS.getMessage() + " " + errMsg);
		}
	}

	/**
	 * Return Panel name.
	 *
	 * @return Panel name.
	 */
	public String getPanelName() {

		return MessageCatalog.SETTINGS_SERVER_TAB.getMessage();

	}

	/**
	 * Changes the elements on the panel according to the selected environment
	 * element. Note that the first element on the list is "Other", so the URL must
	 * be introduced manually.
	 */
	private void comboBoxItemSelected() {
		var k = environmentList.getSelectedIndex();

		if (k == 0) {
			environmentDescription.setVisible(false);
			selectedUrl.setVisible(true);
			selectedUrl.setText(currentUrl);
		} else {
			environmentDescription.setVisible(true);
			selectedUrl.setVisible(false);
			environmentDescription.setText(listEnv.get(k - 1).getDescription());
			selectedUrl.setText(listEnv.get(k - 1).getURL().toString());
		}
	}

	/**
	 * Searches and reads environment configuration files from the classpath.
	 */
	private void readEnvironmentFiles() {
		listEnv = new ArrayList<>();
		var classLoader = Thread.currentThread().getContextClassLoader();

		try {

			var resources = classLoader.getResources(""); //$NON-NLS-1$

			while (resources.hasMoreElements()) {
				var resourceFolder = resources.nextElement();

				var currentResourceFolder = new File(resourceFolder.getPath());

				if (currentResourceFolder.isDirectory()) {
					var listOfResourceFiles = currentResourceFolder
					        .listFiles((dir, nombre) -> nombre.endsWith(ENV_FILE_EXTENSION));
					if (listOfResourceFiles != null) {
						for (File resourceFile : listOfResourceFiles) {
							var envConf = new EnvironmentConfig(resourceFile.getAbsolutePath());
							if (envConf.isValid()) {
								listEnv.add(envConf);
							} else {
								JOptionPane.showMessageDialog(this,
								        MessageCatalog.SETTINGS_SERVER_INVALID_ENV_FILE
								                .getMessage(resourceFile.getAbsolutePath(), envConf.getErrorMessage()),
								        es.ree.eemws.kit.gui.common.MessageCatalog.MSG_ERROR_TITLE.getMessage(),
								        JOptionPane.ERROR_MESSAGE);
							}
						}
					}
				}
			}
		} catch (IOException e) {
			JOptionPane.showMessageDialog(this, MessageCatalog.SETTINGS_SERVER_CANNOT_READ_ENV_FILE.getMessage(),
			        es.ree.eemws.kit.gui.common.MessageCatalog.MSG_ERROR_TITLE.getMessage(), JOptionPane.ERROR_MESSAGE);
		}

		Collections.sort(listEnv, Comparator.comparing(EnvironmentConfig::getTitle));

	}
}
